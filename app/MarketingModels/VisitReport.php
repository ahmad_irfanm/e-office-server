<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class VisitReport extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'visit_report';
    protected $primaryKey = 'id';
    protected $guarded = [];

}
