<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class Level extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'level';
    protected $primaryKey = 'id';
    protected $guarded = [];

}
