<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class Product extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'product';
    protected $primaryKey = 'id';
    protected $guarded = [];

}
