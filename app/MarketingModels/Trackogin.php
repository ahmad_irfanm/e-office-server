<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class Trackogin extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'track_login';
    protected $primaryKey = 'id';
    protected $guarded = [];

}
