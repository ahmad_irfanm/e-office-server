<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class ProsesOrder extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'proses_order';
    protected $primaryKey = 'no_po';
    protected $guarded = [];

}
