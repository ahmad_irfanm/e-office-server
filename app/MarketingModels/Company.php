<?php
namespace App\MarketingModels;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'company';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function customer_contacts () {
        return $this->hasMany(CustomerContact::class, 'company', 'nama');
    }

}
