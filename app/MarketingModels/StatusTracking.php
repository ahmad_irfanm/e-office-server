<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class StatusTracking extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'status_tracking';
    protected $primaryKey = 'id';
    protected $guarded = [];

}
