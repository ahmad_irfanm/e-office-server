<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class User extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $hidden = ['proses_orders'];

    public function visit_reports () {
        return $this->hasMany(VisitReport::class, 'id_user', 'id');
    }

    public function proses_orders () {
        return $this->hasMany(ProsesOrder::class, 'user_id', 'id');
    }

}
