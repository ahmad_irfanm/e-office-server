<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class Merk extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'merk';
    protected $primaryKey = 'id';
    protected $guarded = [];

}
