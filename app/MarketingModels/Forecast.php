<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class Forecast extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'forecasting';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function status_tracking () {
        return $this->hasOne(StatusTracking::class, 'id_forecast', 'id');
    }
}
