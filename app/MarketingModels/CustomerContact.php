<?php
namespace App\MarketingModels;
use App\Marketing\StatusTracking;
use Illuminate\Database\Eloquent\Model;
class CustomerContact extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'customer_contacts';
    protected $primaryKey = 'id';
    protected $guarded = [];

}
