<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Experience extends Model
{
    //
    protected $table = 'tbl_experience';
    protected $guarded = [];
    public $timestamps = false;
}
