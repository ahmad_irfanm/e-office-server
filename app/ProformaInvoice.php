<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class ProformaInvoice extends Model
{
    //
    protected $table = 'tbl_proforma';
    protected $guarded = [];

    public $timestamps = false;
}
