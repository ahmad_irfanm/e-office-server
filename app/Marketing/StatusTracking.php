<?php
namespace App\Marketing;
use App\MarketingModels\Forecast;
use Illuminate\Database\Eloquent\Model;
class StatusTracking extends Model
{
    //
    protected $connection = 'marketing';
    public $timestamps = false;
    protected $table = 'status_tracking';
    protected $primaryKey = 'id';

    public function forecast () {
        return $this->belongsTo(Forecast::class, 'id_forecast', 'id');
    }
}
