<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class LetterCode extends Model
{
    //
    protected $table = 'tbl_code';
    protected $guarded = [];
    public $timestamps = false;
}
