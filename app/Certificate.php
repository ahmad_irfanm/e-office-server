<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Certificate extends Model
{
    //
    protected $table = 'tbl_certificate';
    protected $guarded = [];
    public $timestamps = false;
}
