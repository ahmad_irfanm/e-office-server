<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class LetterNumber extends Model
{
    //
    protected $table = 'tbl_number';
    protected $guarded = [];
    public $timestamps = false;

    public function letter_code () {
        return $this->belongsTo(LetterCode::class, 'code', 'code');
    }
}
