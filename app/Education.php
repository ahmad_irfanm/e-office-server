<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Education extends Model
{
    //
    protected $table = 'tbl_education';
    protected $guarded = [];
    public $timestamps = false;
}
