<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Quotation extends Model
{
    //
    protected $table = 'tbl_quotation';
    protected $guarded = [];

    public $timestamps = false;
}
