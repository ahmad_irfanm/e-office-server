<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class ReminderChecklist extends Model
{
    //
    protected $table = 'tbl_reminder_checklist';
    protected $guarded = [];

    public $timestamps = false;
}
