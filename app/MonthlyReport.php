<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class MonthlyReport extends Model
{
    //
    protected $table = 'tbl_monthly_hutchison';
    protected $guarded = [];
    public $timestamps = false;
}
