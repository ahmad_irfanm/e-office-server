<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class CustomerContact extends Model
{
    //
    protected $table = 'tbl_customer_contact';
    protected $guarded = [];
    public $timestamps = false;
    protected $hidden = ['customer_id'];

    public function customer () {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
