<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TodoList extends Model
{
    //
    protected $table = 'tbl_todo';
    protected $guarded = [];
    public $timestamps = false;

    public function report () {
        return $this->hasOne(Report::class, 'id_todo', 'id');
    }
}
