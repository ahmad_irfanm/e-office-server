<?php
namespace App\AbsentModels;
use App\Position;
use App\User;
use Illuminate\Database\Eloquent\Model;
class Employee extends Model
{
    //
    protected $connection = 'absent';
    public $timestamps = false;
    protected $table = 'emp';
    protected $primaryKey = 'emp_id_auto';

    public function att_logs () {
        return $this->hasMany(AttLog::class, 'pin' , 'pin');
    }

    public function ex_scans () {
        return $this->hasMany(ExScan::class, 'emp_id_auto', 'emp_id_auto');
    }

    public function position () {
        return $this->belongsTo(Position::class, 'id_jabatan', 'id');
    }

    public function user () {
        return $this->hasOne(User::class, 'nik', 'nik');
    }

}
