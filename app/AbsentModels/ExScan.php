<?php
namespace App\AbsentModels;
use Illuminate\Database\Eloquent\Model;
class ExScan extends Model
{
    //
    protected $connection = 'absent';
    public $timestamps = false;
    protected $table = 'ex_scan';
    protected $primaryKey = 'ex_id';

    public function scan_status () {
        return $this->belongsTo(ScanStatus::class, 'scan_id', 'scan_id');
    }
}
