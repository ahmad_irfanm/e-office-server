<?php
namespace App\AbsentMOdels;
use Illuminate\Database\Eloquent\Model;
class ScanStatus extends Model
{
    //
    protected $connection = 'absent';
    public $timestamps = false;
    protected $table = 'scan_status';
    protected $primaryKey = 'scan_id';
}
