<?php
namespace App\AbsentModels;
use Illuminate\Database\Eloquent\Model;
class AttLog extends Model
{
    //
    protected $connection = 'absent';
    public $timestamps = false;
    protected $table = 'att_log';
    protected $primaryKey = 'att_id';

    public function employee () {
        return $this->belongsTo(Employee::class, 'pin', 'pin');
    }
}
