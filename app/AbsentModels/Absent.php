<?php
namespace App\AbsentModels;
use Illuminate\Database\Eloquent\Model;
class Absent extends Model
{
    //
    protected $connection = 'absent';
    public $timestamps = false;
    protected $table = 'absent';
    protected $primaryKey = 'absent_id';
}
