<?php

namespace App\Console\Commands;

use App\Http\Controllers\ReminderController;
use App\Mail\ReminderMail;
use App\Reminder;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendMailReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
//        $data = Reminder::all()[0];
//
//        $purposes = explode('|', $data->tujuan);
//
//        foreach ($purposes as $purpose) {
//            Mail::to($purpose)->send(new ReminderMail($data));
//        }

        // get all reminders
        $reminders = Reminder::all();

        // date now
        $now = date('Y-m-d');

        foreach ($reminders as $reminder) {

            // date start send notif each reminder
            $date_start_notif = date('Y-m-d', strtotime($reminder->tanggal_mulai_notif));

            // when date now greather than date start send notif
            if ($now >= $date_start_notif) {
                $purposes = explode('|', $reminder->tujuan);

                foreach ($purposes as $purpose) {
                    Mail::to($purpose)->send(new ReminderMail($reminder));
                }
            }

        }

    }
}
