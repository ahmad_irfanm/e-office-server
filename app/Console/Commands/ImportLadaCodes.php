<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ImportLadaCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:ladacodes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the .sql file with the area codes of the country.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        DB::unprepared(file_get_contents('database/migrations/intranet_db_administration.sql'));
        DB::unprepared(file_get_contents('database/migrations/tbl_reminder.sql'));
        DB::unprepared(file_get_contents('database/migrations/tbl_reminder_checklist.sql'));
        DB::unprepared(file_get_contents('database/migrations/tbl_reminder_checklist.sql'));

        DB::unprepared("ALTER TABLE tbl_user ADD api_token text DEFAULT NULL");
    }
}
