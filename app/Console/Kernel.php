<?php

namespace App\Console;

use App\Console\Commands\ImportAbsent;
use App\Console\Commands\ImportLadaCodes;
use App\Console\Commands\SendMailReminder;
use App\Mail\ReminderMail;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Facades\Mail;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\ModelMakeCommand::class,
        Commands\ControllerMakeCommand::class,
        ImportLadaCodes::class,
        ImportAbsent::class,
        SendMailReminder::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
        $schedule->command('send:reminder')
            ->everyMinute();

    }
}
