<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Employee extends Model
{
    //
    protected $table = 'tbl_employee';
    protected $guarded = [];
    public $timestamps = false;
    protected $primaryKey = 'nik';

    public function user  () {
        return $this->hasOne(User::class, 'nik', 'nik');
    }

    public function position () {
        return $this->belongsTo(Position::class, 'id_jabatan', 'id');
    }

    public function educations () {
        return $this->hasMany(Education::class, 'id_user', 'nik');
    }

    public function experiences () {
        return $this->hasMany(Experience::class, 'id_user', 'nik');
    }

    public function certificates () {
        return $this->hasMany(Certificate::class, 'id_user', 'nik');
    }

    public function families () {
        return $this->hasMany(Family::class, 'id_user', 'nik');
    }

    public function emergencies () {
        return $this->hasMany(Emergency::class, 'id_user', 'nik');
    }

    public function maritals () {
        return $this->hasMany(Marital::class, 'id_user', 'nik');
    }

}
