<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Position extends Model
{
    //
    protected $table = 'tbl_position';
    protected $guarded = [];
    public $timestamps = false;
}
