<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class ChargedUnit extends Model
{
    //
    protected $table = 'tbl_charged_units';
    protected $guarded = [];
    public $timestamps = false;
}
