<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Family extends Model
{
    //
    protected $table = 'tbl_family';
    protected $guarded = [];
    public $timestamps = false;

    protected $appends = ['age'];

    public function getAgeAttribute () {
        return "";
        $date_from = date_create(substr($this->birth, 6, 4) . '-' . substr($this->birth, 3, 2) . '-' . substr($this->birth, 0, 2));
        $date_to = date_create();

        $diff = date_diff($date_from, $date_to);

        return $diff->y;
    }
}
