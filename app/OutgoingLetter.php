<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class OutgoingLetter extends Model
{
    //
    protected $table = 'tbl_letter_detail';
    protected $guarded = [];
    public $timestamps = false;

    public function number () {
        return $this->belongsTo(LetterNumber::class, 'id_number', 'id');
    }

    public function user () {
        return $this->belongsTo(User::class, 'id_user', 'nik');
    }
}
