<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Request extends Model
{
    //
    protected $table = 'tbl_request';
    protected $guarded = [];
    public $timestamps = false;

    public function driver () {
        return $this->belongsTo(Employee::class, 'id_driver', 'nik');
    }

    public function messenger () {
        return $this->belongsTo(Employee::class, 'id_driver', 'nik');
    }
}
