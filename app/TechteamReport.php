<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class TechteamReport extends Model
{
    //
    protected $table = 'tbl_techteam_report';
    protected $guarded = [];
    public $timestamps = false;
}
