<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class ExecutionID extends Model
{
    //
    protected $table = 'tbl_execution_id';
    protected $guarded = [];
    public $timestamps = false;
}
