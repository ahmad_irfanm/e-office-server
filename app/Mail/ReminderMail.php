<?php

namespace App\Mail;

use App\Reminder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReminderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $reminder;

    public function __construct($data)
    {
        $this->reminder = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $tanggal = date('Y-m-') . date('d', strtotime($this->reminder->tanggal));

        $target = date_create($tanggal);
        $now = date_create(date('Y-m-d'));

        $diff = date_diff($now, $target);

        return $this->view('reminder')
            ->with('reminder', $this->reminder)
            ->with('tanggal', $tanggal)
            ->with('diff', $diff);
    }
}
