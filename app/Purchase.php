<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Purchase extends Model
{
    //
    protected $table = 'tbl_purchase';
    protected $guarded = [];

    public $timestamps = false;

    public function specifications () {
        return $this->hasMany(Specification::class, 'id_purchase', 'id');
    }
}
