<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Track extends Model
{
    //
    protected $table = 'tbl_track';
    protected $guarded = [];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function pic () {
        return $this->belongsTo(User::class, 'pic', 'id');
    }

}
