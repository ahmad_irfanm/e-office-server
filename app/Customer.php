<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Customer extends Model
{
    //
    protected $table = 'tbl_customer';
    protected $guarded = [];
    public $timestamps = false;

    public function customer_contacts () {
        return $this->hasMany(CustomerContact::class, 'customer_id', 'id');
    }
}
