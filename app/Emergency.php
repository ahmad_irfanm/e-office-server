<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Emergency extends Model
{
    //
    protected $table = 'tbl_emergency';
    protected $guarded = [];
    public $timestamps = false;
}
