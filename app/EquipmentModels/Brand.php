<?php
namespace App\EquipmentModels;
use Illuminate\Database\Eloquent\Model;
class Brand extends Model
{
    //
    protected $connection = 'equipment';
    public $timestamps = false;
    protected $table = 'brand';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
