<?php
namespace App\EquipmentModels;
use Illuminate\Database\Eloquent\Model;
class Equipment extends Model
{
    //
    protected $connection = 'equipment';
    public $timestamps = false;
    protected $table = 'equipment';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function brand () {
        return $this->belongsTo(Brand::class, 'brand_id', 'id');
    }
}
