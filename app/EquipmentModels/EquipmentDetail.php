<?php
namespace App\EquipmentModels;
use Illuminate\Database\Eloquent\Model;
class EquipmentDetail extends Model
{
    //
    protected $connection = 'equipment';
    public $timestamps = false;
    protected $table = 'equipment_detail';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function equipment () {
        return $this->belongsTo(Equipment::class);
    }
}
