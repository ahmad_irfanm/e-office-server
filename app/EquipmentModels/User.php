<?php
namespace App\EquipmentModels;
use Illuminate\Database\Eloquent\Model;
class User extends Model
{
    //
    protected $connection = 'equipment';
    public $timestamps = false;
    protected $table = 'user';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
