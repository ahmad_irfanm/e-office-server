<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Reminder extends Model
{
    //
    protected $table = 'tbl_reminder';
    protected $guarded = [];
    public $timestamps = false;

    public function checklists () {
        return $this->hasMany(ReminderChecklist::class, 'id_reminder', 'id');
    }

    public function inventory () {
        return $this->belongsTo(Inventory::class, 'id_inventory', 'id');
    }
}
