<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class PurchasePrinciple extends Model
{
    //
    protected $table = 'tbl_purchase_principle';
    protected $guarded = [];

    public $timestamps = false;

    public function specifications () {
        return $this->hasMany(Specification::class, 'id_purchase_principle', 'id');
    }

    public function customer () {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
