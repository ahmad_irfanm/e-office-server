<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class InvoiceCustomer extends Model
{
    //
    protected $table = 'tbl_invoice';
    protected $guarded = [];
    public $timestamps = false;

    public function customer_data () {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }
}
