<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Job extends Model
{
    //
    protected $table = 'tbl_jobs';
    protected $guarded = [];
    public $timestamps = false;
    protected $primaryKey = 'id';

    public function pic1 () {
        return $this->belongsTo(User::class, 'pic1', 'id');
    }

    public function pic2 () {
        return $this->belongsTo(User::class, 'pic2', 'id');
    }

}
