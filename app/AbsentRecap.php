<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class AbsentRecap extends Model
{
    //
    protected $table = 'tbl_absent_recap';
    protected $guarded = [];
    public $timestamps = false;
}
