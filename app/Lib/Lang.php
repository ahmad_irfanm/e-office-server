<?php


namespace App\Lib;


class Lang
{
    public $messages = [
        'id' => [
            'get_success' => 'Data berhasil di load',
            'store_success' => 'Permitaan Pembuatan Berhasil',
            'update_success' => 'Permintaan Pembaharuan Berhasil',
            'delete_success' => 'Permintaan Penghapusan Berhasil',
        ],
        'en' => [
            'get_success' => 'Data has been loaded',
            'store_success' => 'Data created successful',
            'update_success' => 'Data updated successful',
            'delete_success' => 'Data deleted',
        ]
    ];

    function __construct ($lang) {
        $this->lang = $lang;
    }

    public function get_message($key) {
        return $this->messages[$this->lang][$key];
    }

    public function set_lang ($lang) {
        $this->lang = $lang;
    }
}
