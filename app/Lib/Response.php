<?php


namespace App\Lib;


use Illuminate\Http\Request;

class Response
{

    public static function loaded ($data, $lang = 'en') {
        $lang = new Lang($lang);

        return response()->json([
            'message' => $lang->get_message('get_success'),
            'values' => $data,
        ]);
    }

    public static function stored ($data, $lang = 'en') {
        $lang = new Lang($lang);

        return response()->json([
            'message' => $lang->get_message('store_success'),
            'values' => $data,
        ], 200);
    }

    public static function updated ($data, $lang = 'en') {
        $lang = new Lang($lang);

        return response()->json([
            'message' => $lang->get_message('update_success'),
            'values' => $data,
        ], 200);
    }

    public static function destroyed ($lang = 'en') {
        $lang = new Lang($lang);

        return response()->json([
            'message' => $lang->get_message('delete_success')
        ], 200);
    }

}
