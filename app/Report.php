<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Report extends Model
{
    //
    protected $table = 'tbl_report';
    protected $guarded = [];
    public $timestamps = false;

    public function todolist () {
        return $this->belongsTo(TodoList::class, 'id_todo', 'id');
    }
}
