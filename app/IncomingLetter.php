<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class IncomingLetter extends Model
{
    //
    protected $table = 'tbl_letter_income';
    protected $guarded = [];
    public $timestamps = false;
}
