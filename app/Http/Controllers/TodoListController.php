<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\TodoList;
use Illuminate\Http\Request;
use Validator;

class TodoListController extends Controller
{
    //
    function __construct(TodoList $todoList) {
        $this->todoList = $todoList;
    }

    public function index ($date) {
        $todoLists = $this->todoList
            ->where('date', date('d-m-Y', strtotime($date)))
            ->where('id_user', auth()->user()->id)
            ->get()
            ->map(function ($todo) {
                $todo->start = date('H:i', strtotime($todo->start));
                $todo->end = date('H:i', strtotime($todo->end));
                return $todo;
            });

        return response()->json([
            'message' => 'Data has been loaded',
            'values' => $todoLists,
        ], 200);
    }

    public function show ($id) {
        $todoList = $this->todoList->where('id', $id)->with(['report'])->first();

        return response()->json([
            'message' => 'Data has been loaded',
            'values' => $todoList,
        ], 200);
    }

    public function store (Request $request) {
        $time_now = date('H:i:s');

        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'todo_list' => 'required',
        ]);

        $date_now = date('d-m-Y', strtotime($request->date));

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid field',
                'errors' => $validator->errors(),
            ], 401);

        $this->todoList->create([
            'date' => $date_now,
            'time' => $time_now,
            'start' => $date_now . ' ' . $request->start_time,
            'end' => $date_now . ' ' . $request->end_time,
            'todo_list' => $request->todo_list,
            'id_user' => auth()->user()->id,
            'id' => $this->todoList->orderBy('id', 'desc')->first()->id + 1,
        ]);

        return response()->json([
            'message' => 'Todo List created successful',
        ], 200);
    }

    public function update (Request $request, $id) {
        $item = $this->todoList->find($id);

        $date_now = date('d-m-Y', strtotime($item->date));
        $time_now = date('H:i:s');

        $validator = Validator::make($request->all(), [
            'start_time' => 'required',
            'end_time' => 'required',
            'todo_list' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid field',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'date' => $date_now,
            'time' => $time_now,
            'start' => $date_now . ' ' . $request->start_time,
            'end' => $date_now . ' ' . $request->end_time,
            'todo_list' => $request->todo_list,
        ]);

        return response()->json([
            'message' => 'Todo List updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->todoList->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Todo List deleted successful',
        ], 200);
    }
}
