<?php
namespace App\Http\Controllers\EquipmentControllers;
use App\EquipmentModels\Equipment;
use App\EquipmentModels\EquipmentDetail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class EquipmentController extends Controller
{
    //
    public function __construct(Equipment $item, EquipmentDetail $detail)
    {
        //
        $this->item = $item;
        $this->detail = $detail;
    }

    public function index (Request $request) {
        $year = $request->year ? $request->year : date('Y');

        $items = $this->item
            ->orderBy('id', 'asc')
            ->with(['brand'])
            ->get();

        return response()->json($items, 200);
    }

    public function delivered (Request $request) {
        $details = $this->detail->with(['equipment' => function ($equipment) {
            return $equipment->with(['brand'])->get();
        }]);

        if ($request->filter_by == 'location') {
            $details = $details->where('location', $request->location);
        } elseif ($request->filter_by == 'brand') {
            $equipments = $this->item->where('brand_id', $request->brand_id)->get()->map(function ($equipment) {
                return $equipment->id;
            });
            $details = $details->whereIn('equipment_id', $equipments);
        } elseif ($request->filter_by == 'equipment') {
            $details = $details->where('equipment_id', $request->equipment_id);
        }

        $details = $details->get();

        return response()->json($details, 200);
    }

    public function location () {
        $locations = $this->detail->orderBy('location', 'asc')
            ->get()
            ->map(function ($detail) {
                return $detail->location;
            })
            ->unique()
            ->values();

        return response()->json($locations, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
            'equip_name' => 'required',
            'picture' => 'file|mimes:png,jpg,jpeg',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/equipments/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = 'null';
        }

        $this->item->create([
            'brand_id' => $request->brand_id,
            'equip_name' => $request->equip_name,
            'picture' => $file,
        ]);


        return response()->json([
            'message' => 'Equipment created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'brand_id' => 'required',
            'equip_name' => 'required',
            'picture' => 'file|mimes:png,jpg,jpeg',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/equipments/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $item->picture;
        }

        $item->update([
            'brand_id' => $request->brand_id,
            'equip_name' => $request->equip_name,
            'picture' => $file,
        ]);

        return response()->json([
            'message' => 'Equipment updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Equipment deleted successful',
        ], 200);
    }
}
