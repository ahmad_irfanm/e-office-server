<?php
namespace App\Http\Controllers\EquipmentControllers;
use App\EquipmentModels\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class BrandController extends Controller
{
    //
    public function __construct(Brand $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $items = $this->item
            ->orderBy('brand_name', 'asc')
            ->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'brand_name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id' => $this->item->orderBy('id', 'desc')->first()->id + 1,
            'brand_name' => $request->brand_name,
        ]);

        return response()->json([
            'message' => 'Brand created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'brand_name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'brand_name' => $request->brand_name,
        ]);

        return response()->json([
            'message' => 'Brand updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Brand deleted successful',
        ], 200);
    }
}
