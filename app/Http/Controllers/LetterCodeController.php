<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\LetterCode;
use Illuminate\Http\Request;

class LetterCodeController extends Controller
{
    //
    function __construct (LetterCode $letterCode) {
        $this->letterCode = $letterCode;
    }

    public function index () {
        $letterCodes = $this->letterCode->orderBy('name', 'asc')->get();

        return response()->json([
            'message' => 'Load data successful',
            'values' => $letterCodes
        ], 200);
    }
}
