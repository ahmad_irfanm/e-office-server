<?php
namespace App\Http\Controllers;
use App\TechteamReport;
use Validator;
use Illuminate\Http\Request;
class TechteamReportController extends Controller
{
    //
    public function __construct(TechteamReport $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $items = $this->item
            ->whereBetween('date', [$request->start, $request->end])
            ->orderBy('date', 'desc')
            ->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'technical' => 'required',
            'date' => 'required',
            'time' => 'required',
            'duration' => 'required',
            'activity' => 'required',
            'subject' => 'required',
            'description' => 'required',
            'id_user' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id' => $this->item->orderBy('id', 'desc')->first()->id + 1,
            'technical' => $request->technical,
            'date' => $request->date,
            'time' => $request->time,
            'duration' => $request->duration,
            'activity' => $request->activity,
            'subject' => $request->subject,
            'description' => $request->description,
            'id_user' => $request->id_user,
        ]);

        return response()->json([
            'message' => 'Techteam Report created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'technical' => 'required',
            'date' => 'required',
            'time' => 'required',
            'duration' => 'required',
            'activity' => 'required',
            'subject' => 'required',
            'description' => 'required',
            'id_user' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'technical' => $request->technical,
            'date' => $request->date,
            'time' => $request->time,
            'duration' => $request->duration,
            'activity' => $request->activity,
            'subject' => $request->subject,
            'description' => $request->description,
            'id_user' => $request->id_user,
        ]);

        return response()->json([
            'message' => 'Techteam Report updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Techteam Report deleted successful',
        ], 200);
    }
}
