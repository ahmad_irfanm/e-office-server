<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Inventory;
use Illuminate\Http\Request;
class InventoryController extends Controller
{
    //
    function __construct (Inventory $inventory) {
        $this->inventory = $inventory;
    }

    public function index () {
        $inventories = $this->inventory->orderBy('name', 'asc')->get();

        return response()->json([
            'message' => 'Inventories loaded',
            'values' => $inventories
        ], 200);
    }
}
