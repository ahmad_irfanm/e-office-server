<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Lib\Response;
use App\Mail\ReminderMail;
use App\Reminder;
use App\ReminderChecklist;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Mail;


class ReminderController extends Controller
{
    //
    function __construct (Reminder $reminder, ReminderChecklist $reminderChecklist, User $user) {
        $this->reminder = $reminder;
        $this->reminderChecklist = $reminderChecklist;
        $this->user = $user;

        $this->lang = 'id';
    }

    public function index (Request $request) {
        $reminders = $this->reminder->orderBy('tanggal', 'asc')->get()->map(function($reminder) {
            $reminder->active = $reminder->tanggal > date('Y-m-d');

            $tanggal = date_create($reminder->tanggal);
            $tanggal_mulai_notif = date_create($reminder->tanggal_mulai_notif);

            $diff = date_diff($tanggal, $tanggal_mulai_notif);

            $reminder->tanggal = date('d/m/Y', strtotime($reminder->tanggal));
            $reminder->tanggal_mulai_notif = 'h-' . (($diff->m * 30) + $diff->d);

            return $reminder;
        });

        return Response::loaded($reminders, $this->lang);
    }

    public function show (Request $request, $id) {
        // define reminder
        $reminder = $this->reminder->find($id);

        $reminder->active = $reminder->tanggal > date('Y-m-d');

        return Response::loaded($reminder, $this->lang);
    }

    // OLD
    public function show_ ($id) {
        $reminder = $this->reminder->find($id);

        $start_notif = date_create($reminder->tanggal_mulai_notif);
        $date = date_create($reminder->tanggal);
        $diff = date_diff($start_notif, $date);

        $reminder->diff = $diff->d;

        $detail = [];
        $now = date_create(date('Y-m-d'));

        $diff = date_diff($date, $now);
        $range = $diff->m + 1; // for the next reminder

        foreach (range(0, $range-1) as $month) {
            $detail[] = date('Y-m-d', strtotime('+' . $month . ' months', strtotime($reminder->tanggal)));
        }

        // reminder detail
        $reminder->detail = $detail;

        // selection reminder detail
        $reminder->detail = collect($reminder->detail)->map(function($item) use ($reminder) {
            $reminder_checklist = $this->reminderChecklist
                ->where('id_reminder', $reminder->id)
                ->where('tanggal', $item)->first();

            return [
                'date' => date('Y-m-d', strtotime($item)),
                'day' => date('d', strtotime($item)),
                'month' => date('F', strtotime($item)),
                'year' => date('Y', strtotime($item)),
                'reminder_checklist' => $reminder_checklist ? $reminder_checklist : null
            ];
        });

        return response()->json([
            'message' => 'Reminder data loaded',
            'values' => $reminder
        ]);
    }

    // get all reminder histories
    public function get_histories (Request $request) {
        $histories = $this->reminderChecklist->orderBy('tanggal_update', 'desc')->get()->map(function ($i) {
            $i->tanggal_reminder = date('d/m/Y', strtotime($i->tanggal_reminder));
            $i->tanggal_update = date('d/m/Y H:i', strtotime($i->tanggal_update));

            return $i;
        });

        return Response::loaded($histories, $this->lang);
    }

    public function remove_histories (Request $request) {

        $validator = Validator::make($request->all(), [
           'reminder_history_ids' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Error'
            ]);

        $reminder_history_ids = json_decode($request->reminder_history_ids);

        if (!is_array($reminder_history_ids)) {
            return "Reminder History IDs must be array";
        }

        // selection reminder history ids
        foreach ($reminder_history_ids as $reminder_history_id) {
            if (!$this->reminderChecklist->find($reminder_history_id)) {
                return response()->json([
                    'message' => 'Reminder History ID '. $reminder_history_id .' Not found',
                ], 401);
            }
        }

        foreach ($reminder_history_ids as $reminder_history_id) {
            $this->reminderChecklist->find($reminder_history_id)->delete();
        }

        return response()->json([
            'message' => 'Reminder Histories Deleted Successful',
        ], 200);

    }

    // OLD
    public function check (Request $request, $id) {
        $user = $this->user->where('api_token', $request->bearerToken())->first();

        $reminderChecklist = $this->reminderChecklist->create([
            'id_reminder' => $id,
            'tanggal' => $request->date,
            'id_user' => $user->id
        ]);

        if ($reminderChecklist) {
            return Response::updated($reminderChecklist, $request->header('lang'));
        }
    }

    // create reminder
    public function store (Request $request) {

        // set validator
        $validator = Validator::make($request->all(), [
            'deskripsi' => 'required',
            'memo' => 'required',
            'tanggal' => 'required',
            'tujuan' => 'required|array',
            'tanggal_mulai_notif' => 'required'
        ]);

        // validator fails handle
        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid field',
                'values' => $validator->errors()
            ], 401);

        // modify request purpose
        if ($request->tujuan) {
            $tujuan = collect($request->tujuan)->join('|');
        }

        // insert reminder
        $reminder = $this->reminder->create([
            'deskripsi' => $request->deskripsi,
            'memo' => $request->memo,
            'repeat_days' => $request->repeat_days,
            'repeat_months' => $request->repeat_months,
            'repeat_years' => $request->repeat_years,
            'tanggal' => $request->tanggal,
            'tanggal_mulai_notif' => $request->tanggal_mulai_notif,
            'tujuan' => $tujuan,
        ]);

        // response success
        if ($reminder) {
            return response()->json(['message' => 'Reminder berhasil dibuat'], 200);
        }

    }

    // update reminder
    public function update (Request $request, $id) {

        // define reminder
        $reminder = $this->reminder->find($id);

        // set validator
        $validator = Validator::make($request->all(), [
            'deskripsi' => 'required',
            'memo' => 'required',
            'tanggal' => 'required',
            'tujuan' => 'required',
            'tanggal_mulai_notif' => 'required'
        ]);

        // validator fails handle
        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid field',
                'values' => $validator->errors()
            ], 401);

        // modify request purpose
        if ($request->tujuan) {
            $tujuan = collect($request->tujuan)->join('|');
        }

        // set checked
        $checked = $request->checked === "0" || $request->checked == "1" ? $request->checked : $reminder->checked;

        // updating reminder
        $update = $reminder->update([
            'deskripsi' => $request->deskripsi,
            'memo' => $request->memo,
            'repeat_days' => $request->repeat_days,
            'repeat_months' => $request->repeat_months,
            'repeat_years' => $request->repeat_years,
            'tanggal' => $request->tanggal,
            'tanggal_mulai_notif' => $request->tanggal_mulai_notif,
            'tujuan' => $tujuan,
            'checked' => $checked
        ]);

        // if checklist reminder
        if ($request->checklist) {
            $this->reminderChecklist->create([
                'deskripsi' => $request->deskripsi,
                'memo' => $request->memo,
                'tanggal_reminder' => $request->tanggal,
                'tanggal_update' => Carbon::now(),
            ]);
        }

        // response success
        if ($update) {
            return Response::updated($reminder, $this->lang);
        }

    }

    // send mail
    public function send_mail () {

        // get all reminders
        $reminders = $this->reminder->get();

        // date now
        $now = date('Y-m-d');

        Mail::to('email.ahmadirfan@gmail.com')->send(new ReminderMail($reminders[0]));

        // response success
        return response()->json([
            'message' => 'successful'
        ], 200);

        foreach ($reminders as $reminder) {

            // date start send notif each reminder
            $date_start_notif = date('Y-m-d', strtotime($reminder->tanggal_mulai_notif));

            // when date now greather than date start send notif
            if ($now >= $date_start_notif) {
                $purposes = explode('|', $reminder->tujuan);

                foreach ($purposes as $purpose) {
                    Mail::to($purpose)->send(new ReminderMail($reminder));
                }
            }

        }

        // response success
        return response()->json([
            'message' => 'successful'
        ], 200);

    }

    // notification
    public function notification () {
        // get all reminders
        $reminders = $this->reminder->where('checked', 0)->orderBy('tanggal', 'desc')->get();

        // date now
        $now = date('Y-m-d');

        $results = [];

        $i = 0;
        foreach ($reminders as $reminder) {

            // date start send notif each reminder
            $date_start_notif = date('Y-m-d', strtotime($reminder->tanggal_mulai_notif));

            // when date now greather than date start send notif
            if ($now >= $date_start_notif) {
                $results[$i]['id'] = $reminder->id;
                $results[$i]['msg'] = $reminder->memo;
                $results[$i]['date'] = date('l, j F Y', strtotime($reminder->tanggal));

                $i++;
            }

        }

        // response success
        return response()->json($results, 200);
    }

    // destroy reminder
    public function destroy (Request $request, $id) {

        $reminder = $this->reminder->find($id);

        $delete = $reminder->delete();

        if ($delete) {
            return Response::destroyed($this->lang);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed'
            ], 422);
        }
    }
}
