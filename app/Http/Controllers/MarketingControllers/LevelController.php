<?php

namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;

use App\MarketingModels\CustomerContact;
use App\MarketingModels\Level;
use App\MarketingModels\VisitReport;
use Illuminate\Http\Request;
use Validator;

class LevelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Level $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $year = $request->year ? $request->year : date('Y');

        $items = $this->item
            ->orderBy('level', 'asc')
            ->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'level' => 'required',
            'akses' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'level' => $request->level,
            'akses' => $request->akses,
        ]);


        return response()->json([
            'message' => 'Level created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'level' => 'required',
            'akses' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'level' => $request->level,
            'akses' => $request->akses,
        ]);

        return response()->json([
            'message' => 'Level updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Level deleted successful',
        ], 200);
    }
}
