<?php

namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;

use App\MarketingModels\Company;
use App\MarketingModels\CustomerContact;
use Illuminate\Http\Request;
use Validator;

class CustomerContactController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomerContact $item)
    {
        //
        $this->item = $item;
    }

    public function overview () {
        $companies = Company::orderBy('nama', 'asc')->get()->map(function ($company) {
            return [
                'company' => $company->nama,
                'customer_contacts' => $company->customer_contacts->count(),
            ];
        })->filter(function ($company) {
            return $company['customer_contacts'] > 0;
        })->values();

        return response()->json($companies, 200);
    }

    public function index (Request $request) {
        $items = $this->item->orderBy('company', 'asc');

        if ($request->company) {
            $items = $items->where('company', $request->company);
        }

        $items = $items->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'nama_karyawan' => 'required',
            'divisi' => 'required',
            'posisi' => 'required',
            'company' => 'required',
            'jenis_kelamin' => 'required',
            'telp' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'nama_karyawan' => $request->nama_karyawan,
            'divisi' => $request->divisi,
            'posisi' => $request->posisi,
            'company' => $request->company,
            'jenis_kelamin' => $request->jenis_kelamin,
            'telp' => $request->telp,
            'nik' => $request->nik,
            'no_ktp' => $request->no_ktp,
            'no_npwp' => $request->no_npwp,
            'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
            'alamat' => $request->alamat,
            'email' => $request->email,
        ]);


        return response()->json([
            'message' => 'Customer contact created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'nama_karyawan' => 'required',
            'divisi' => 'required',
            'posisi' => 'required',
            'company' => 'required',
            'jenis_kelamin' => 'required',
            'telp' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'nama_karyawan' => $request->nama_karyawan,
            'divisi' => $request->divisi,
            'posisi' => $request->posisi,
            'company' => $request->company,
            'jenis_kelamin' => $request->jenis_kelamin,
            'telp' => $request->telp,
            'nik' => $request->nik,
            'no_ktp' => $request->no_ktp,
            'no_npwp' => $request->no_npwp,
            'tempat_tanggal_lahir' => $request->tempat_tanggal_lahir,
            'alamat' => $request->alamat,
            'email' => $request->email,
        ]);


        return response()->json([
            'message' => 'Customer contact updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Customer contact deleted successful',
        ], 200);
    }
}
