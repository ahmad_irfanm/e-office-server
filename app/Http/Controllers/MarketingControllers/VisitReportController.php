<?php

namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;

use App\MarketingModels\CustomerContact;
use App\MarketingModels\User;
use App\MarketingModels\VisitReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class VisitReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VisitReport $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $year = $request->year ? $request->year : date('Y');

        $items = $this->item
            ->orderBy('jadwal', 'asc')
            ->whereYear('jadwal', $year)
            ->where('tipe', $request->tipe)
            ->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {

        $user = auth()->user();
        $user = User::where('username', $user->username)->first();

        $rules = [
            'jadwal' => 'required',
            'company' => 'required',
            'nama_contact' => 'required',
            'lokasi' => 'required',
            'pembahasan' => 'required',
            'kesimpulan' => 'required',
            'upload' => 'file',
            'tipe' => 'required',
        ];

        if ($request->tipe == 'mom') {
            $rules['project_name'] = 'required';
        } elseif ($request->tipe == 'progress') {
            $rules['project_name'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $file = $request->file('upload');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/sales/visit-reports';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = null;
        }

        $this->item->create([
            'jadwal' => $request->jadwal,
            'company' => $request->company,
            'nama_contact' => $request->nama_contact,
            'company' => $request->company,
            'lokasi' => $request->lokasi,
            'pembahasan' => $request->pembahasan,
            'kesimpulan' => $request->kesimpulan,
            'upload' => $file,
            'tipe' => $request->tipe,
            'nama_project' => $request->nama_project,
            'subjek' => $request->subjek,
            'input' => Carbon::now(),
            'id_user' => $user->id,
            'nama_user' => $user->nama,
        ]);


        return response()->json([
            'message' => 'Report created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $rules = [
            'jadwal' => 'required',
            'company' => 'required',
            'nama_contact' => 'required',
            'lokasi' => 'required',
            'pembahasan' => 'required',
            'kesimpulan' => 'required',
            'upload' => 'file',
            'tipe' => 'required',
        ];

        if ($request->tipe == 'mom') {
            $rules['project_name'] = 'required';
        } elseif ($request->tipe == 'progress') {
            $rules['project_name'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $file = $request->file('upload');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/sales/visit-reports';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $item->upload;
        }

        $item->update([
            'jadwal' => $request->jadwal,
            'company' => $request->company,
            'nama_contact' => $request->nama_contact,
            'company' => $request->company,
            'lokasi' => $request->lokasi,
            'pembahasan' => $request->pembahasan,
            'kesimpulan' => $request->kesimpulan,
            'upload' => $file,
            'nama_project' => $request->nama_project,
            'subjek' => $request->subjek,
        ]);


        return response()->json([
            'message' => 'Report updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Report deleted successful',
        ], 200);
    }
}
