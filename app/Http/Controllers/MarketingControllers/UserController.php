<?php

namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;

use App\MarketingModels\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {

        $items = $this->item
            ->orderBy('nama', 'asc')
            ->get();

        return response()->json($items, 200);
    }

    public function statistic () {

        $items = $this->item
            ->whereIn('jabatan', ['Technical Support', 'Account Manager'])
            ->orderBy('nama', 'asc')
            ->get()
            ->map(function ($item) {
               $item->visit = $item->visit_reports()->where('tipe', 'visit')->count();
               $item->mom = $item->visit_reports()->where('tipe', 'mom')->count();
               $item->progress = $item->visit_reports()->where('tipe', 'progress')->count();
               $item->proses_order = $item->proses_orders->count();

                return [
                    'nama' => $item->nama,
                    'visit' => $item->visit,
                    'mom' => $item->mom,
                    'progress' => $item->progress,
                    'proses_order' => $item->proses_order,
                ];
            });

        return response()->json($items, 200);

    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required|regex:/^[a-z0-9_]+$/',
            'password' => 'required|confirmed',
            'jabatan' => 'required',
            'role' => 'required',
            'nama' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item = $this->item->create([
            'username' => $request->username,
            'password' => $request->password,
            'jabatan' => $request->jabatan,
            'role' => $request->role,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'telepon' => $request->telepon,
            'email' => $request->email,
        ]);


        return response()->json([
            'message' => 'User created successful',
            'values' => $item,
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Account not found',
            ], 404);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Account not found',
            ], 404);

        $validator = Validator::make($request->all(), [
            'username' => 'required|regex:/^[a-z0-9_]+$/',
            'jabatan' => 'required',
            'role' => 'required',
            'nama' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'username' => $request->username,
            'jabatan' => $request->jabatan,
            'role' => $request->role,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'telepon' => $request->telepon,
            'email' => $request->email,
        ]);

        return response()->json([
            'message' => 'User updated successful',
            'values' => $item,
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Account not found',
            ], 404);

        $item->delete();

        return response()->json([
            'message' => 'User deleted successful',
        ], 200);
    }
}
