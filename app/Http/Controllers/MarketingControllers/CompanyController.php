<?php

namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;

use App\MarketingModels\Company;
use Illuminate\Http\Request;
use Validator;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Company $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $items = $this->item->orderBy('nama', 'asc');

        if ($request->mode == 'complex') {
            $items = $items->with(['customer_contacts']);
        }

        $items = $items->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'warna' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'nama' => $request->nama,
            'warna' => $request->warna,
            'alamat' => $request->alamat,
        ]);

        return response()->json([
            'message' => 'Company created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->with(['customer_contacts'])->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        if (!$item) {
            return response()->json([
                'message' => 'Comapny not found',
            ], 401);
        }

        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'warna' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'nama' => $request->nama,
            'warna' => $request->warna,
            'alamat' => $request->alamat,
        ]);


        return response()->json([
            'message' => 'Company updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        if (!$item) {
            return response()->json([
                'message' => 'Comapny not found',
            ], 401);
        }

        $item->delete();

        return response()->json([
            'message' => 'Company deleted successful',
        ], 200);
    }
}
