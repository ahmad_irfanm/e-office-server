<?php

namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;

use App\MarketingModels\CustomerContact;
use App\MarketingModels\Level;
use App\MarketingModels\Merk;
use App\MarketingModels\VisitReport;
use Illuminate\Http\Request;
use Validator;

class MerkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Merk $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {

        $items = $this->item
            ->orderBy('nama', 'asc')
            ->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item = $this->item->create([
            'nama' => $request->nama,
            'ket' => $request->ket,
            'id_product' => $request->id_product,
        ]);


        return response()->json([
            'message' => 'Merk created successful',
            'values' => $item,
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Merk not found',
            ], 404);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Merk not found',
            ], 404);

        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'nama' => $request->nama,
            'ket' => $request->ket,
            'id_product' => $request->id_product,
        ]);

        return response()->json([
            'message' => 'Merk updated successful',
            'item' => $item,
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Merk not found',
            ], 404);

        $item->delete();

        return response()->json([
            'message' => 'Merk deleted successful',
        ], 200);
    }
}
