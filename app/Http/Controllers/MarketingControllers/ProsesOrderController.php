<?php

namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;

use App\MarketingModels\CustomerContact;
use App\MarketingModels\Forecast;
use App\MarketingModels\Level;
use App\MarketingModels\ProsesOrder;
use App\User;
use App\MarketingModels\VisitReport;
use Illuminate\Http\Request;
use Validator;

class ProsesOrderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProsesOrder $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $year = $request->year ? $request->year : date('Y');

        $items = $this->item
            ->orderBy('tanggal_po', 'desc')
            ->where('tipe', $request->tipe);

        if ($request->year) {
            $items = $items->whereYear('tanggal_po', $request->year);
        }

        $items = $items->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $user = auth()->user();

        $rules = [
            'tanggal_po' => 'required',
            'price_currency' => 'required',
            'price_value' => 'required',
            'delivery_date' => 'required',
            'attachment' => 'file|mimes:pdf,docx',
            'tipe' => 'required|in:from_forecast,not_from_forecast',
        ];

        if (!$user)
            $rules['id_user'] = 'required';

        if ($request->tipe == 'from_forecast') {
            $rules['id_forecast'] = 'required';
            $rules['po_number'] = 'required';
        } else {
            $rules['customer_name'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $forecast = Forecast::find($request->id_forecast);

        if (!$user)
            $user = \App\MarketingModels\User::where('nik', $request->id_user)->first();

        if (!$user)
            return response()->json([
                'message' => 'User not found',
            ], 401);

        if (!$forecast && $request->tipe == 'from_forecast')
            return response()->json([
                'message' => 'Project Not Found',
            ], 401);

        elseif ($request->tipe == 'not_from_forecast')  {
            $forecast['id'] = NULL;
            $forecast['project_name'] = NULL;
        }

        $file = $request->file('attachment');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/sales/purchase-orders';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = null;
        }

        $this->item->create([
            'tanggal_po' => $request->tanggal_po,
            'po_number' => $request->po_number ? $request->po_number : 0,
            'projek' => $forecast['project_name'],
            'id_forecast' => $forecast['id'],

            'nilai' => $request->price_currency . '. ' . $request->price_value,
            'delivery_date' => $request->delivery_date,
            'upload' => $file,

            'user_id' => $user->id,
            'customer' => $request->customer_name,

            'tipe' => $request->tipe,
            'status' => NULL,
        ]);


        return response()->json([
            'message' => 'Purchase created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'tanggal_po' => 'required',
            'id_forecast' => 'required',
            'po_number' => 'required',
            'price_currency' => 'required',
            'price_value' => 'required',
            'delivery_date' => 'required',
            'attachment' => 'file|mimes:pdf,docx',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $forecast = Forecast::find($request->id_forecast);

        if (!$forecast)
            return response()->json([
                'message' => 'Project Not Found',
            ], 401);

        $file = $request->file('attachment');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/sales/purchase-orders';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $item->upload;
        }

        $item->update([
            'tanggal_po' => $request->tanggal_po,
            'po_number' => $request->po_number,
            'projek' => $forecast->project_name,
            'id_forecast' => $forecast->id,

            'nilai' => $request->price_currency . '. ' . $request->price_value,
            'delivery_date' => $request->delivery_date,
            'upload' => $file,

            'customer' => NULL,

            'tipe' => $request->tipe,
            'status' => NULL,
        ]);

        return response()->json([
            'message' => 'Purchase updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Purchase deleted successful',
        ], 200);
    }
}
