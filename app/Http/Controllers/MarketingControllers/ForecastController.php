<?php
namespace App\Http\Controllers\MarketingControllers;
use App\Http\Controllers\Controller;
use App\MarketingModels\Forecast;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class ForecastController extends Controller
{
    //
    function __construct (Forecast $forecast) {
        $this->item = $forecast;
    }

    public function index ($year) {
        $forecasts = $this->item->orderBy('date_create', 'asc')->whereYear('date_create', $year)->get();

        return response()->json([
            'message' => 'Data has been loaded',
            'values' => $forecasts,
        ], 200);
    }

    public function show ($id) {
        $forecast = $this->item->where('id', $id)->with(['status_tracking'])->get();

        return response()->json([
            'message' => 'Data has been loaded',
            'values' => $forecast,
        ], 200);
    }

    public function store (Request $request) {
        $user = auth()->user();

        $rules = [
            'company_name' => 'required',
            'project_name' => 'required',
            'merk' => 'required',
            'product' => 'required',
            'qty' => 'required|numeric',
            'total_price' => 'required',
            'date' => 'required',
        ];

        if (!$user) {
            $rules['id_user'] = 'required';
        }

        $validator = Validator::make($request->all(), $rules);

        if (!$user)
            $user = User::where('nik', $request->id_user)->first();

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id_user' => $user->id,
            'company_name' => $request->company_name,
            'project_name' => $request->project_name,
            'merk' => $request->merk,
            'product' => $request->product,
            'qty' => $request->qty,
            'total_price' => $request->total_price,
            'unit_price' => $request->unit_price,
            'status' => '1',
            'date_create' => $request->date,
        ]);

        return response()->json([
            'message' => 'Forecast created successful',
        ], 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

//        $rules = [
//            'company_name' => 'required',
//            'project_name' => 'required',
//            'merk' => 'required',
//            'product' => 'required',
//            'qty' => 'required|numeric',
//            'total_price' => 'required',
//            'date' => 'required',
//        ];
//
//        $validator = Validator::make($request->all(), $rules);
//
//        if ($validator->fails())
//            return response()->json([
//                'message' => 'Invalid fields',
//                'errors' => $validator->errors(),
//            ], 401);

        $item->update([
            'company_name' => $request->company_name,
            'project_name' => $request->project_name,
            'merk' => $request->merk,
            'product' => $request->product,
            'qty' => $request->qty,
            'unit_price' => $request->unit_price,
            'total_price' => $request->total_price,
            'date_create' => $request->date,
            'status' => $request->status,
        ]);

        return response()->json([
            'message' => 'Forecast updated successful',
            'data' => $item,
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Forecast deleted successful',
        ], 200);
    }

    public function graph_bar () {

        $graph = [];

        $duration = range(2015, date('Y'));

        $forecast = collect($duration)->map(function ($year) {
            return Forecast::whereYear('date_create', $year)->count();
        });

        $graph['series'] = [
            [
                'name' => 'Forecast',
                'data' => $forecast,
            ]
        ];

        $graph['categories'] = $duration;

        return response()->json($graph, 200);

    }
}
