<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\PurchasePrinciple as Purchase;
use App\Specification;
use Illuminate\Http\Request;
use Validator;

class PurchasePrincipleController extends Controller
{
    //
    function __construct (Purchase $purchase, Specification $specification) {
        $this->purchase = $purchase;
        $this->specification = $specification;
    }

    public function index (Request $request) {
        $purchases = $this->purchase->orderBy('id', 'desc');

        if ($request->year) {
            $purchases = $purchases->whereYear('date', $request->year);
        }

        $purchases = $purchases->get()->map(function ($i) {

            // change format date
            $i->date = date('d/m/Y', strtotime($i->date));

            return $i;

        });;

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $purchases
        ], 200);
    }

    public function show ($id) {
        $purchase = $this->purchase
            ->where('id', $id)
            ->with(['specifications', 'customer'])
            ->first();

        // change format date
        $purchase->tanggal_display = date('d/m/Y', strtotime($purchase->date));

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $purchase
        ], 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'po_number' => 'required',
            'principle_name' => 'required',
            'project_name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        $create = $this->purchase->create([
            'date' => $request->date,
            'po_number' => $request->po_number,
            'principle_name' => $request->principle_name,
            'project_name' => $request->project_name,
            'customer_id' => $request->customer_id,
        ]);

        if ($request->specifications) {
            foreach ($request->specifications as $specification) {
                $specification = $this->specification->create([
                    'id_purchase_principle' => $create->id,
                    'name' => $specification['name'],
                    'qty' => $specification['qty'],
                    'currency' => $specification['currency'],
                    'price' => $specification['price'],
                    'total' => $specification['total']
                ]);
            }
        }

        if ($create) {
            return response()->json([
                'message' => 'Purchase Principle Created Successful',
                'values' => $create
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }

    }

    public function upload (Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:pdf,png,docx',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);
        }

        $file = $request->file('file');

        $original_filename = $file->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $ext = end($original_filename_arr);
        $destination_path = './upload/purchases/';
        $file_name = 'purchase_' . $id . '.' . $ext;
        $upload_file = $file->move($destination_path, $file_name);
        $file = $file_name;

        $this->purchase->find($id)->update([
            'file' => $file,
        ]);

        // check upload file sop
        if (!$upload_file) {
            return response()->json([
                'msg' => 'upload file error!'
            ], 401);
        }

        return response()->json([
            'message' => 'File uploaded successful',
        ], 200);

    }

    public function update (Request $request, $id) {
        $purchase = $this->purchase->find($id);

        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'po_number' => 'required',
            'principle_name' => 'required',
            'project_name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        $update = $purchase->update([
            'date' => $request->date,
            'po_number' => $request->po_number,
            'project_name' => $request->project_name,
            'principle_name' => $request->principle_name,
            'customer_id' => $request->customer_id,
        ]);

        if ($request->specifications) {
            foreach ($purchase->specifications as $specification) {
                $specification->delete();
            }

            foreach ($request->specifications as $specification) {
                $specification = $this->specification->create([
                    'id_purchase_principle' => $purchase->id,
                    'name' => $specification['name'],
                    'qty' => $specification['qty'],
                    'currency' => $specification['currency'],
                    'price' => $specification['price'],
                    'total' => $specification['total']
                ]);
            }
        }

        if ($update) {
            return response()->json([
                'message' => 'Purchase Principle Created Successful',
                'values' => $purchase
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }

    }

    public function destroy ($id) {
        $item = $this->purchase->find($id);

        $delete = $item->delete();

        if ($delete) {
            return response()->json([
                'message' => 'Purchase Order to Principle Deleted Successful'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }
}
