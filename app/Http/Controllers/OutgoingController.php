<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\LetterNumber;
use App\OutgoingLetter;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class OutgoingController extends Controller
{
    //
    function __construct(OutgoingLetter $outgoing, LetterNumber $letterNumber)
    {
        $this->outgoing = $outgoing;
        $this->letterNumber = $letterNumber;
    }

    public function index (Request $request) {
        $outgoings = $this->outgoing->with(['number', 'user' => function ($user) {
            return $user->with(['employee'])->get();
        }]);

        $outgoings = $outgoings->orderBy('id', 'desc')->get()->map(function ($o) {

            // change format date
            $o->tanggal = date('d/m/Y', strtotime($o->tanggal));
            $o->tanggal_po = date('d/m/Y', strtotime($o->tanggal_po));

            return $o;

        });;

        if ($request->year) {
            $year = $request->year;
            $outgoings = collect($outgoings)->filter(function ($outgoing) use ($year) {
                return $outgoing->number->year == $year;
            })->values()->all();
        }

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $outgoings
        ], 200);
    }

    public function show ($id) {
        $outgoing = $this->outgoing->where('id', $id)->with(['number', 'user' => function ($user) {
            return $user->with(['employee'])->get();
        }])->first();

        $outgoing->tanggal_display = date('d/m/Y', strtotime($outgoing->tanggal));
        $outgoing->tanggal_po_display = date('d/m/Y', strtotime($outgoing->tanggal_po));

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $outgoing
        ], 200);
    }

    public function get_last_number (Request $request) {
        $year = $request->year ? $request->year : date('Y');

        $last = $this->letterNumber
            ->where('year', $year)
            ->orderBy('number', 'desc')
            ->first();

        $romawi = ["I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII"];

        if (!$last) {
            $last = new \stdClass();
            $last->year = $year;
            $last->number = 0;
            $last->month = $romawi[date('n') - 1];
        }

        return response()->json($last, 200);
    }

    public function store(Request $request) {
        $rules = [
            "code" => "required",
            "dari" => "required",
            "id_user" => "required",
            "keterangan" => "required",
            "lokasi_file" => "required",
            "nomor_po" => "required",
            "note_refer_to" => "required",
            "perihal" => "required",
            "tanggal" => "required",
            "untuk" => "required",
            "up" => "required",
        ];

        // validator
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'message' => 'Field invalid',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/outgoing-letters/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = 'null';
        }

        // generate number
        $number = $this->letterNumber
            ->orderBy('number', 'desc')
            ->where('year', $request->year ? $request->year : date('Y'))
            ->first();

        if ($number) $number = $number->number + 1;
        else $number = 1;

        $romawi = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII'];
        $last = $this->letterNumber->orderBy('id', 'desc')->first();
        $number_id = $last ? $last->id + 1 : 1;

        $number = $this->letterNumber->create([
            'id' => $number_id,
            'number' => $number,
            'code' => $request->code,
            'month' => $request->year && $request->year != date('Y') ? 'XII' : $romawi[date('m') - 1],
            'year' => $request->year ? $request->year : date('Y'),
        ]);

        // generate ID
        $last = $this->outgoing->orderBy('id', 'desc')->first();
        $id = $last ? $last->id + 1 : 1;

        $create = $this->outgoing->create([
            'id' => $id,
            'id_number' => $number_id,
            'dari' => $request->dari,
            'id_user' => $request->id_user,
            'keterangan' => $request->keterangan,
            'lokasi_file' => $request->lokasi_file,
            'nomor_po' => $request->nomor_po,
            'note_refer_to' => $request->note_refer_to,
            'perihal' => $request->perihal,
            'tanggal' => $request->tanggal,
            'untuk' => $request->untuk,
            'up' => $request->up,
            'file' => $file
        ]);

        if ($create) {
            return response()->json([
                'message' => 'Outgoing Letter Created Successful',
                'values' => $create
            ], 200);
        } else {
            $number->delete();

            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function update(Request $request, $id) {
        $outgoing = $this->outgoing->find($id);

        $rules = [
            "code" => "required",
            "dari" => "required",
            "keterangan" => "required",
            "lokasi_file" => "required",
            "nomor_po" => "required",
            "note_refer_to" => "required",
            "perihal" => "required",
            "tanggal" => "required",
            "untuk" => "required",
            "up" => "required",
        ];

        if ($request->type == 'upload') {
            $rules = [
                'file' => 'required|file',
            ];
        }

        // validator
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'message' => 'Field invalid',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/outgoing-letters/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $outgoing->file;
        }

        $id_user = null;
        if (!$request->id_user) {
            $id_user = $outgoing->id_user;
        } else {
            $id_user = $request->id_user;
        }

        if ($request->type != 'upload') {
            $update = $outgoing->update([
                'dari' => $request->dari,
                'id_user' => $request->id_user,
                'keterangan' => $request->keterangan,
                'lokasi_file' => $request->lokasi_file,
                'nomor_po' => $request->nomor_po,
                'note_refer_to' => $request->note_refer_to,
                'perihal' => $request->perihal,
                'tanggal' => $request->tanggal,
                'tanggal_po' => $request->tanggal_po,
                'untuk' => $request->untuk,
                'up' => $request->up,
                'file' => $file
            ]);

            if ($update) {
                return response()->json([
                    'message' => 'Outgoing Letter Updated Successful',
                    'values' => $outgoing
                ], 200);
            } else {
                return response()->json([
                    'message' => 'Data cannot be processed',
                ], 422);
            }
        } else {
            return response()->json([
                'message' => 'Outgoing Letter Updated Successful'
            ], 200);
        }

    }

    public function destroy ($id) {
        $item = $this->outgoing->find($id);

        $number = $this->letterNumber->find($item->id_number);
        if ($number)
            $number->delete();

        $delete = $item->delete();

        if ($delete) {
            return response()->json([
                'message' => 'Outgoing Letter Deleted Successful'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }
}
