<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

class CertificateController extends Controller
{
    //
    public function __construct(\App\Certificate $item)
    {
        //
        $this->item = $item;
    }

    public function store (Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $file = $request->file('file');

        $original_filename = $file->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $ext = end($original_filename_arr);
        $destination_path = './upload/certificates/';
        $file_name = 'U-' . time() . '.' . $ext;
        $upload_file = $file->move($destination_path, $file_name);
        $file = $file_name;

        // check upload file sop
        if (!$upload_file) {
            return response()->json([
                'msg' => 'upload file error!'
            ], 401);
        }

        // generate ID
        $last = $this->item->orderBy('id', 'desc')->first();
        $id2 = $last ? $last->id + 1 : 1;

        $this->item->create([
            'id' => $id2,
            'id_user' => $id,
            'file' => $file,
        ]);

        return response()->json([
            'message' => 'Certificate uploaded successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Certificate deleted successful',
        ], 200);
    }
}
