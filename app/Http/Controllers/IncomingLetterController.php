<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\IncomingLetter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class IncomingLetterController extends Controller
{
    //
    function __construct (IncomingLetter $incomingLetter) {
        $this->incomingLetter = $incomingLetter;
    }

    public function index(Request $request) {

        $incomingLetters = $this->incomingLetter->orderBy('id', 'desc');

        if ($request->year) {
            $incomingLetters = $incomingLetters->whereYear('tanggal_terima', $request->year);
        }

        $incomingLetters = $incomingLetters->get()->map(function ($i) {

            // change format date
            $i->tanggal_surat = date('d/m/Y', strtotime($i->tanggal_surat));
            $i->tanggal_terima = date('d/m/Y', strtotime($i->tanggal_terima));

            return $i;

        });

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $incomingLetters
        ], 200);
    }

    public function show($id) {
        $incomingLetter = $this->incomingLetter->where('id', $id)->first();

        // change format date

        if ($incomingLetter->tanggal_surat)
            $incomingLetter->tanggal_surat_display = date('d/m/Y', strtotime($incomingLetter->tanggal_surat));
        else
            $incomingLetter->tanggal_surat_display = '-';

        $incomingLetter->tanggal_terima_display = date('d/m/Y', strtotime($incomingLetter->tanggal_terima));

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $incomingLetter
        ], 200);
    }

    public function store(Request $request) {
        // validator
        $validator = Validator::make($request->all(), [
            'tanggal_terima' => 'required',
            'penerima' => 'required',
            'nomor_surat' => 'required',
            'nama_perusahaan' => 'required',
            'cp' => 'required',
            'telp_fax' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Field invalid',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/incoming-letters/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = 'null';
        }

        // generate ID
        $last = $this->incomingLetter->orderBy('id', 'desc')->first();
        $id = $last ? $last->id + 1 : 1;

        $create = $this->incomingLetter->create([
            'id' => $id,
            'tanggal_terima' => $request->tanggal_terima,
            'tanggal_surat' => $request->tanggal_surat,
            'penerima' => $request->penerima,
            'nomor_surat' => $request->nomor_surat,
            'nama_perusahaan' => $request->nama_perusahaan,
            'cp' => $request->cp,
            'telp_fax' => $request->telp_fax,
            'alamat' => $request->alamat,
            'perihal' => $request->perihal,
            'page' => $request->page,
            'tujuan_surat' => $request->tujuan_surat,
            'lokasi_file' => $request->lokasi_file,
            'file' => $file
        ]);

        if ($create) {
            return response()->json([
                'message' => 'Incoming Letter Created Successful',
                'values' => $create
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function update(Request $request, $id) {
        $incomingLetter = $this->incomingLetter->find($id);

        // validator
        $validator = Validator::make($request->all(), [
            'tanggal_terima' => 'required',
            'penerima' => 'required',
            'nomor_surat' => 'required',
            'nama_perusahaan' => 'required',
            'cp' => 'required',
            'telp_fax' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Field invalid',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/incoming-letters/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;



            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $incomingLetter->file;
        }

        $update = $incomingLetter->update([
            'tanggal_surat' => $request->tanggal_surat,
            'tanggal_terima' => $request->tanggal_terima,
            'penerima' => $request->penerima,
            'nomor_surat' => $request->nomor_surat,
            'nama_perusahaan' => $request->nama_perusahaan,
            'cp' => $request->cp,
            'telp_fax' => $request->telp_fax,
            'alamat' => $request->alamat,
            'perihal' => $request->perihal,
            'page' => $request->page,
            'tujuan_surat' => $request->tujuan_surat,
            'lokasi_file' => $request->lokasi_file,
            'file' => $file
        ]);

        if ($update) {
            return response()->json([
                'message' => 'Incoming Letter Update Successful',
                'values' => $incomingLetter
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function destroy ($id) {
        $item = $this->incomingLetter->find($id);

        $delete = $item->delete();

        if ($delete) {
            return response()->json([
                'message' => 'Incoming Letter Deleted Successful'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }
}
