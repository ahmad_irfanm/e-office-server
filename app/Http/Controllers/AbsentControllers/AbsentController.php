<?php
namespace App\Http\Controllers\AbsentControllers;
use App\AbsentModels\Absent;
use App\AbsentModels\Employee;
use App\AbsentModels\ExScan;
use App\AbsentRecap;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;

class AbsentController extends Controller
{
    function __construct (Absent $absent, Employee $employee, User $user, ExScan $exScan, AbsentRecap $absentRecap) {
        $this->absent = $absent;
        $this->employee = $employee;
        $this->user = $user;
        $this->exScan = $exScan;
        $this->absentRecap = $absentRecap;
    }
    //
    public function get_punctuality_chart (Request $request, $start_date, $end_date) {

        // user ids except
        $user_ids_except = [1, 4, 6, 7];

        // filter jabatan
        if ($request->position) {
            $jabatan_ids = [$request->position];
        } else {
            $jabatan_ids = [1, 2, 3, 4, 5, 6, 7];
        }

        // get niks of all employee
        $employee_niks = $this->user
            ->whereNotIn('id', $user_ids_except)
            ->where('active', '1')
            ->with(['employee' => function ($employee) use ($jabatan_ids) {
                $employee->whereIn('id_jabatan', $jabatan_ids)->get();
            }])->get()
            ->filter(function ($user) {
                return $user->employee;
            })
            ->map(function ($user) {
                return $user->employee->nik;
            });

        // get employees data with attendance logs
        $employees = $this->employee
            ->whereIn('nik', $employee_niks)
            ->with(['att_logs' => function ($att_log) use ($start_date, $end_date) {
                if ($start_date == $end_date) {
                    $att_log->whereDate('scan_date', $start_date)->get();
                } else {
                    $att_log->whereDate('scan_date', '>=', $start_date)->whereDate('scan_date', '<=', $end_date)->get();
                }
            }])
            ->orderBy('first_name', 'asc')
            ->get();


        // result format
        $result = [];

        // put series
        $result['series'] = [
            [
                'name' => 'Tepat Waktu',
                'data' => $employees->map(function ($employee) {
                    // optimize
                    $optimize = $this->optimize($employee->att_logs, $employee->emp_id_auto);

                    return collect($optimize)->filter(function ($scan) {
                        return $scan['time_come'] <= '08:00:00';
                    })->count();
                }),
            ],
            [
                'name' => 'Telat',
                'data' => $employees->map(function ($employee) {
                    // optimize
                    $optimize = $this->optimize($employee->att_logs, $employee->emp_id_auto);

                    return collect($optimize)->filter(function ($scan) {
                        return $scan['time_come'] > '08:00:00' && !isset($scan['ex_scan']);
                    })->count();
                }),
            ],
            [
                'name' => 'Izin Telat',
                'data' => $employees->map(function ($employee) {
                    // optimize
                    $optimize = $this->optimize($employee->att_logs, $employee->emp_id_auto);

                    return collect($optimize)->filter(function ($scan) {
                        return $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 10 ? true : false) : false;
                    })->count();
                }),
            ],
            [
                'name' => 'Izin Cepat Pulang',
                'data' => $employees->map(function ($employee) {
                    // optimize
                    $optimize = $this->optimize($employee->att_logs, $employee->emp_id_auto);

                    return collect($optimize)->filter(function ($scan) {
                        return $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 11 ? true : false) : false;
                    })->count();
                }),
            ]
        ];

        // put categories
        $user = $this->user;
        $result['categories'] = $employees->map(function ($employee) use ($user) {
           return strtolower($user->where('nik', $employee->nik)->first()->username);
        });

        // response
        return $result;

    }

    public function get_employee_scan ($nik, $year) {
        // define employee
        $employee = $this->employee->where('nik', $nik)->with(['att_logs' => function ($att_log) use ($year) {
            return $att_log->whereYear('scan_date', $year)->get();
        }])->first();

        // response
        return collect($this->optimize($employee->att_logs, $employee->emp_id_auto))->filter(function ($item) use ($year) {
            return date('Y', strtotime($item['date'])) == $year && date('D', strtotime($item['date'])) != 'Sat' && date('D', strtotime($item['date'])) != 'Sun';
        })->map(function ($item) {
            $item['time_come'] = substr($item['time_come'], 0, 5);
            $item['time_leave'] = substr($item['time_leave'], 0, 5);

            return $item;
        });
    }

    public function get_employee_pie ($nik, $year) {
        // define employee
        $employee = $this->employee->where('nik', $nik)->with(['att_logs' => function ($att_log) use ($year) {
            return $att_log->whereYear('scan_date', $year)->get();
        }])->first();

        // optimize scans
        $optimize = collect($this->optimize($employee->att_logs, $employee->emp_id_auto));

        // define result
        $result = [];

        // tepat waktu
        $result['labels'][] = "Tepat waktu";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['time_come'] <= '08:00:00';
        })->count();

        // telat
        $result['labels'][] = "Telat";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['time_come'] > '08:00:00' && !isset($scan['ex_scan']);
        })->count();

        // izin telat
        $result['labels'][] = "Izin telat";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 10 ? true : false) : false;
        })->count();

        // izin pulang cepat
        $result['labels'][] = "Izin pulang cepat";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 11 ? true : false) : false;
        })->count();

        // resonse
        return $result;

    }

    public function get_employee_punctuality ($nik, $year) {

        // define employee
        $employee = $this->employee->where('nik', $nik)->with(['att_logs' => function ($att_log) use ($year) {
            return $att_log->whereYear('scan_date', $year)->get();
        }])->first();

        // define optimize
        $optimize = $this->optimize($employee->att_logs, $employee->emp_id_auto);

        $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        $result = [];

        $result['categories'] = collect($months)->map(function ($month) use ($year) {
            return date('M', strtotime(date($year . '-' . $month . '-' . '01')));
        });

        $result['series'] = [
            [
                'name' => 'On Time',
                'data' => collect($months)->map(function ($month) use ($year, $optimize) {
                    $date = date('Y-m', strtotime($year . '-' . $month));
                   return collect($optimize)->filter(function ($scan) use ($date) {
                       return date('Y-m', strtotime($scan['date'])) == $date && $scan['time_come'] <= '08:00:00';
                   })->count();
                }),
            ],
            [
                'name' => 'Telat',
                'data' => collect($months)->map(function ($month) use ($year, $optimize) {
                    $date = date('Y-m', strtotime($year . '-' . $month));
                    return collect($optimize)->filter(function ($scan) use ($date) {
                        return date('Y-m', strtotime($scan['date'])) == $date && $scan['time_come'] > '08:00:00' && !isset($scan['ex_scan']);
                    })->count();
                }),
            ],
            [
                'name' => 'Izin Telat',
                'data' => collect($months)->map(function ($month) use ($year, $optimize) {
                    $date = date('Y-m', strtotime($year . '-' . $month));
                    return collect($optimize)->filter(function ($scan) use ($date) {
                        return date('Y-m', strtotime($scan['date'])) == $date && $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 10 ? true : false) : false;
                    })->count();
                }),
            ],
            [
                'name' => 'Izin Cepat Pulang',
                'data' => collect($months)->map(function ($month) use ($year, $optimize) {
                    $date = date('Y-m', strtotime($year . '-' . $month));
                    return collect($optimize)->filter(function ($scan) use ($date) {
                        return date('Y-m', strtotime($scan['date'])) == $date && $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 11 ? true : false) : false;
                    })->count();
                }),
            ]
        ];

        return $result;

    }

    public function get_employee_timecame ($nik, $year) {
        // define employee
        $employee = $this->employee->where('nik', $nik)->with(['att_logs' => function ($att_log) use ($year) {
            return $att_log->whereYear('scan_date', $year)->get();
        }])->first();

        // optimize
        $optimize = collect($this->optimize($employee->att_logs, $employee->emp_id_auto))->sortBy('date')->values()->all();

        $result = [];

        $result['categories'] = collect($optimize)->map(function ($scan) {
           return $scan['date'];
        });

        $result['series'] = [
            [
                'name' => 'Time come',
                'data' => collect($optimize)->map(function ($scan) {
                    return  $scan['time_come'] && $scan['time_come'] != '-' ? $scan['time_come'] : 0;
                })
            ],
            [
                'name' => 'Jam telat',
                'data' => collect($optimize)->map(function ($scan) {
                    return date('H:i:s', strtotime('08:00:00'));
                })
            ],
        ];

        return $result;
    }

    public function not_present (Request $request) {
        $year = $request->year;
        $month = $request->month;
        $now = date('Y-m-d');

        $niks = $this->user->where('active', '1')->get()->map(function ($user) {
            return $user->nik;
        });

        $start_date = date('Y-m-d', strtotime("$year-$month-01"));
        $dates = [];
        foreach (range(1, 31) as $item) {
            if (strlen($item == 1))
                $item = "0" . $item;

            $new_date = date('Y-m-d', strtotime("$year-$month-$item"));

            if (date('m', strtotime($start_date)) == date('m', strtotime($new_date))) {
                $dates[] = $new_date;
            }
        }

        $dates = collect($dates)->filter(function ($date) use ($now) {
           return $date <= $now;
        });

        $employees = $this->employee
            ->whereIn('nik', $niks)
            ->whereNotIn('first_name', ['Ira', 'Fatkhur', 'M'])
            ->with(['att_logs' => function ($att_log) use ($year, $month) {
                return $att_log
                    ->whereYear('scan_date', $year)
                    ->whereMonth('scan_date', $month)
                    ->get();
            }])
            ->get()
            ->map(function ($employee) use ($dates) {
                $optimize = collect($this->optimize($employee->att_logs, $employee->emp_id_auto))->map(function ($att) {
                    return $att['date'];
                });

                $not_present_date = collect($dates)->filter(function ($date) use ($optimize) {
                    return !$optimize->contains($date);
                })->values();

                return [
                    'employee' => collect($employee)->except(['att_logs']),
                    'not_present_date' => $not_present_date,
                ];
            });

        $employees = $employees->map(function ($employee) {
           return collect($employee['not_present_date'])->map(function ($not_present_date) use ($employee) {

               $absent_recap = AbsentRecap::where('nik', $employee['employee']['nik'])->where('tanggal', $not_present_date)->first();
               $status = $absent_recap ? $absent_recap->status : 'Alfa';
               $description = $absent_recap ? $absent_recap->description : null;

               return [
                   'date' => $not_present_date,
                   'day' => date('l', strtotime($not_present_date)),
                   'employee' => [
                       'nik' => $employee['employee']['nik'],
                       'name' => $employee['employee']['first_name'] . ' ' . $employee['employee']['last_name'],
                   ],
                   'status' => $status,
                   'description' => $description,
               ];
           });
        });

        $results = [];

        foreach ($employees as $employee) {
            foreach ($employee as $not_present_date) {
                if (date('D', strtotime($not_present_date['date'])) != 'Sun' && date('D', strtotime($not_present_date['date'])) != 'Sat') {
                    $results[] = $not_present_date;
                }
            }
        }

        $results = collect($results)->sortByDesc('date')->values();

        return response()->json([
            'month' => $month,
            'year' => $year,
            'not_present_date' => $results,
        ]);

    }

    public function not_present_chart_peryear_ ($request) {
        $year = $request->year;

        $niks = $this->user->where('active', '1')->get()->map(function ($user) {
            return $user->nik;
        });

        $start_date = date('Y-m-d', strtotime("$year-01-01"));

        $dates = [];
        foreach (range(1, 31) as $item) {
            if (strlen($item == 1))
                $item = "0" . $item;

            $new_date = date('Y-m-d', strtotime("$year-01-$item"));

            if (date('m', strtotime($start_date)) == date('m', strtotime($new_date))) {
                $dates[] = $new_date;
            }
        }

        $now = date('Y-m-d');

        $dates = collect($dates)->filter(function ($date) use ($now) {
            return $date <= $now;
        });

        $employees = $this->employee
            ->whereIn('nik', $niks)
            ->whereNotIn('first_name', ['Ira', 'Fatkhur', 'M'])
            ->with(['att_logs' => function ($att_log) use ($year) {
                return $att_log
                    ->whereYear('scan_date', $year)
                    ->get();
            }])
            ->get()
            ->map(function ($employee) use ($dates) {
                $optimize = collect($this->optimize($employee->att_logs, $employee->emp_id_auto))->map(function ($att) {
                    return $att['date'];
                });

                $not_present_date = collect($dates)
                    ->filter(function ($date) use ($optimize) {
                        return !$optimize->contains($date);
                    })
                    ->filter(function ($date) {
                        return date('D', strtotime($date)) != 'Sun' && date('D', strtotime($date)) != 'Sat';
                    })
                    ->map(function ($date) use ($employee) {
                        $absent_recap = AbsentRecap::where('nik', $employee->nik)->where('tanggal', $date)->first();
                        $status = $absent_recap ? $absent_recap->status : 'Alfa';
                        $description = $absent_recap ? $absent_recap->description : null;

                        return [
                            'date' => $date,
                            'status' => $status,
                            'description' => $description,
                        ];
                    })->values();



                return [
                    'employee' => collect($employee)->except(['att_logs']),
                    'not_present_date' => $not_present_date,
                ];
            });

        $results = [];

        $results['series'] = [
            [
                'name' => 'Alfa',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Alfa';
                    })->count();
                }),
            ],
            [
                'name' => 'Sakit',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Sakit';
                    })->count();
                }),
            ],
            [
                'name' => 'Izin',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Izin';
                    })->count();
                }),
            ],
            [
                'name' => 'Cuti',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Cuti';
                    })->count();
                }),
            ],
        ];

        $results['categories'] = collect($employees)->map(function ($employee) {
            return $employee['employee']['first_name'] . $employee['employee']['last_name'];
        });

        return $results;
    }

    public function not_present_chart_peryear ($request) {
        $year = $request->year;

        $niks = $this->user->where('active', '1')->get()->map(function ($user) {
            return $user->nik;
        });

        $date = "$year-01-01";
        $first_date_in_year = date_create($date);
        $now = date_create();

        $diff_first_year_now = date_diff($first_date_in_year, $now);

        $range_first_year_now = $diff_first_year_now->m * 30 + $diff_first_year_now->d;

        $dates = [];
        foreach (range(1, $range_first_year_now) as $item) {

            if ($date <= date('Y-m-d')) {
                $dates[] = $date;
            }

            $date = date('Y-m-d', strtotime('+1 days', strtotime($date)));
        }

        $employees = $this->employee
            ->whereIn('nik', $niks)
            ->whereNotIn('first_name', ['Ira', 'Fatkhur', 'M'])
            ->with(['att_logs' => function ($att_log) use ($year) {
                return $att_log
                    ->whereYear('scan_date', $year)
                    ->get();
            }])
            ->get()
            ->map(function ($employee) use ($dates) {
                $optimize = collect($this->optimize($employee->att_logs, $employee->emp_id_auto))->map(function ($att) {
                    return $att['date'];
                });

                $not_present_date = collect($dates)
                    ->filter(function ($date) use ($optimize) {
                        return !$optimize->contains($date);
                    })
                    ->filter(function ($date) {
                        return date('D', strtotime($date)) != 'Sun' && date('D', strtotime($date)) != 'Sat';
                    })
                    ->map(function ($date) use ($employee) {
                        $absent_recap = AbsentRecap::where('nik', $employee->nik)->where('tanggal', $date)->first();
                        $status = $absent_recap ? $absent_recap->status : 'Alfa';
                        $description = $absent_recap ? $absent_recap->description : null;

                        return [
                            'date' => $date,
                            'status' => $status,
                            'description' => $description,
                        ];
                    })->values();



                return [
                    'employee' => collect($employee)->except(['att_logs']),
                    'not_present_date' => $not_present_date,
                ];
            });

        $results = [];

        $results['series'] = [
            [
                'name' => 'Alfa',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Alfa';
                    })->count();
                }),
            ],
            [
                'name' => 'Sakit',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Sakit';
                    })->count();
                }),
            ],
            [
                'name' => 'Izin',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Izin';
                    })->count();
                }),
            ],
            [
                'name' => 'Cuti',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Cuti';
                    })->count();
                }),
            ],
        ];

        $results['categories'] = collect($employees)->map(function ($employee) {
            return $employee['employee']['first_name'] . $employee['employee']['last_name'];
        });

        return $results;
    }

    public function not_present_chart (Request $request) {


        $year = $request->year;
        $month = $request->month;

        if (!$month)
            return $this->not_present_chart_peryear($request);

        $niks = $this->user->where('active', '1')->get()->map(function ($user) {
            return $user->nik;
        });

        $start_date = date('Y-m-d', strtotime("$year-$month-01"));

        $dates = [];
        foreach (range(1, 31) as $item) {
            if (strlen($item == 1))
                $item = "0" . $item;

            $new_date = date('Y-m-d', strtotime("$year-$month-$item"));

            if (date('m', strtotime($start_date)) == date('m', strtotime($new_date))) {
                $dates[] = $new_date;
            }
        }

        $now = date('Y-m-d');

        $dates = collect($dates)->filter(function ($date) use ($now) {
            return $date <= $now;
        });

        $employees = $this->employee
            ->whereIn('nik', $niks)
            ->whereNotIn('first_name', ['Ira', 'Fatkhur', 'M'])
            ->with(['att_logs' => function ($att_log) use ($year, $month) {
                return $att_log
                    ->whereYear('scan_date', $year)
                    ->whereMonth('scan_date', $month)
                    ->get();
            }])
            ->get()
            ->map(function ($employee) use ($dates) {
                $optimize = collect($this->optimize($employee->att_logs, $employee->emp_id_auto))->map(function ($att) {
                    return $att['date'];
                });

                $not_present_date = collect($dates)
                    ->filter(function ($date) use ($optimize) {
                        return !$optimize->contains($date);
                    })
                    ->filter(function ($date) {
                        return date('D', strtotime($date)) != 'Sun' && date('D', strtotime($date)) != 'Sat';
                    })
                    ->map(function ($date) use ($employee) {
                    $absent_recap = AbsentRecap::where('nik', $employee->nik)->where('tanggal', $date)->first();
                    $status = $absent_recap ? $absent_recap->status : 'Alfa';
                    $description = $absent_recap ? $absent_recap->description : null;

                    return [
                        'date' => $date,
                        'status' => $status,
                        'description' => $description,
                    ];
                })->values();



                return [
                    'employee' => collect($employee)->except(['att_logs']),
                    'not_present_date' => $not_present_date,
                ];
            });

        $results = [];

        $results['series'] = [
            [
                'name' => 'Alfa',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                       return $date['status'] == 'Alfa';
                    })->count();
                }),
            ],
            [
                'name' => 'Sakit',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Sakit';
                    })->count();
                }),
            ],
            [
                'name' => 'Izin',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Izin';
                    })->count();
                }),
            ],
            [
                'name' => 'Cuti',
                'data' => collect($employees)->map(function ($employee) {
                    return collect($employee['not_present_date'])->filter(function ($date) {
                        return $date['status'] == 'Cuti';
                    })->count();
                }),
            ],
        ];

        $results['categories'] = collect($employees)->map(function ($employee) {
            return $employee['employee']['first_name'] . $employee['employee']['last_name'];
        });

        return $results;

    }

    public function update_status_not_present (Request $request) {
        $validator = Validator::make($request->all(), [
            'nik' => 'required',
            'tanggal' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $employee = $this->employee->where('nik', $request->nik)->first();

        if (!$employee)
            return response()->json([
                'message' => 'Employee not found',
            ], 404);

        $absent_recap = $this->absentRecap->where('nik', $request->nik)->where('tanggal', $request->tanggal)->first();

        if ($absent_recap) {
            $absent_recap->update([
                'status' => $request->status,
                'description' => $request->description,
            ]);
        } else {
            $absent_recap = $this->absentRecap->create([
                'nik' => $request->nik,
                'tanggal' => $request->tanggal,
                'status' => $request->status,
                'description' => $request->description,
            ]);
        }

        return response()->json([
            'message' => 'Status updated successful',
            'values' => $absent_recap,
        ], 200);

    }

    public function optimize ($att_logs, $employee_id) {

        $exScan = $this->exScan;
        $duplicates = collect([]);

        $employee = Employee::find($employee_id);

        $att_logs->each(function ($att_log) use ($duplicates) {
            $day = date('D', strtotime($att_log->scan_date));
            if ($day != 'Sat' && $day != 'Sun') {
                $scan_date = date('Y-m-d', strtotime($att_log->scan_date));
                if (!$duplicates->contains($scan_date)) {
                    $duplicates->push($scan_date);
                }
            }
        });


        $chunk = $duplicates->map(function ($date) use ($att_logs, $exScan, $employee) {
            // define item
            $item = [];

            $atts = $att_logs->filter(function ($att_log) use ($date) {
                return date('Y-m-d', strtotime($att_log->scan_date)) == $date;
            });

            $item['date']= $date;

            $item['time_come'] = $atts->filter(function ($att) {

                // khusus ramadhan
                $now = date('Y-m-d');
                if ($now >= '2021-04-13' && $now < '2021-05-13') {
                    return date('H:i:s', strtotime($att->scan_date)) < '16:30:00';
                }

               return date('H:i:s', strtotime($att->scan_date)) < '17:00:00';
            })->sortBy('scan_date')->first();

            $item['time_leave'] = $atts->filter(function ($att) {

                // khusus ramadhan
                $now = date('Y-m-d');
                if ($now >= '2021-04-13' && $now < '2021-05-13') {
                    return date('H:i:s', strtotime($att->scan_date)) >= '16:30:00';
                }

               return date('H:i:s', strtotime($att->scan_date)) >= '17:00:00';
            })->sortByDesc('scan_date')->first();

            $item['duration'] = '-';

            if ($item['time_come'] && $item['time_leave']) {
                $create_time_come = date_create($item['time_come']->scan_date);
                $create_time_leave = date_create($item['time_leave']->scan_date);

                $diff = date_diff($create_time_come, $create_time_leave);
                $item['duration'] = $diff->h . ' jam ' . $diff->i . ' menit.';
            }

            $days = [
                'Mon' => 'Senin',
                'Tue' => 'Selasa',
                'Wed' => 'Rabu',
                'Thu' => 'Kamis',
                'Fri' => "Jum'at",
                'Sat' => 'Sabtu',
                'Sun' => 'Minggu'
            ];

            $item['day'] = $item['time_come'] ? $days[date('D', strtotime($item['time_come']->scan_date))] : '-';

            $item['time_come'] = $item['time_come'] ? date('H:i:s', strtotime($item['time_come']->scan_date)) : '-';
            $item['time_leave'] = $item['time_leave'] ? date('H:i:s', strtotime($item['time_leave']->scan_date)) : '-';



            // get ex scans
            $item['ex_scan'] = $exScan->where('emp_id_auto', $employee->emp_id_auto)->with(['scan_status'])->whereDate('tgl_scan', $date)->orderBy('lastupdate_date', 'desc')->first();

            return $item;

        })->sortByDesc('date')->values()->all();

        return $chunk;
    }

    public function test () {
        DB::connection('backup')->unprepared("INSERT INTO absent VALUES(13, 'Test')");
        return $this->absent->get();
    }

    public function upload (Request $request) {

        $validator = Validator::make($request->all(), [
            'sql' => 'required',
        ]);

//        return $request->sql;

//        if ($validator->fails())
//            return $validator->errors();

//        $avatar = Str::random(34);
//        $request->file('sql')->move(storage_path('backup'), $avatar . '.sql');
        $sql = $request->file('sql');

        $contents = file_get_contents($sql->getRealPath());
//
        try {
            DB::connection('absent')->unprepared($contents);

            if ($request->redirect) {
                return redirect($request->redirect);
            } else {
                return response()->json([
                    'message' => 'SQL file has been uploaded',
                ], 200);
            }
        } catch (\Exception $e) {
            die("Could not");
        }
//
    }

}
