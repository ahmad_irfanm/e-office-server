<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class FamilyController extends Controller
{
    //
    public function __construct(\App\Family $item)
    {
        //
        $this->item = $item;
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'name' => 'required',
            'relation' => 'required',
            'birth' => 'required',
            'work' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id_user' => $request->id_user,
            'name' => $request->name,
            'relation' => $request->relation,
            'birth' => $request->birth,
            'work' => $request->work,
            'address' => $request->address,
        ]);

        return response()->json([
            'message' => 'Family created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'relation' => 'required',
            'birth' => 'required',
            'work' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'name' => $request->name,
            'relation' => $request->relation,
            'birth' => $request->birth,
            'work' => $request->work,
            'address' => $request->address,
        ]);

        return response()->json([
            'message' => 'Family updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Family deleted successful',
        ], 200);
    }
}
