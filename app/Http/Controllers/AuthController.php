<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Validator;

class AuthController extends Controller
{
    //
    function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login (Request $request) {
        // init
        $response = ['success' => true, 'msg' => null, 'userData' => null];
        $status_code = 200;

        // get user where email
        $user = $this->user->with(['employee' => function ($employee) {
            $employee->with(['position'])->get();
        }])->where('username', $request->input('username'))->first();


        // if email doesn't exists
        if (!$user) {
            $response['success'] = false;
            $response['msg'] = 'Username not found';
            $status_code = 401;
        }

        // if email exists
        else {

            // generate token
            $token = app('auth')->setTTL(30)->attempt($request->only('username', 'password'));

            // if password match
            if ($token) {

                // put token on user data
                $update_user = $user->update([
                    'api_token' => $token
                ]);
                unset($user->api_token);

                $user['expires_in'] = auth()->factory()->getTTL() * 60;

                // set response success
                $response['message'] = 'Login successful';
                $response['userData'] = $user;
                $response['accessToken'] = $token;

            }

            // if password doesn't match
            else {
                $response['success'] = false;
                $response['message'] = 'Password incorect';
                $status_code = 401;
            }

        }


        // return response
        return response()->json($response, $status_code);

    }

    public function change_password (Request $request) {
        $user = auth()->user();

        $validator = Validator::make($request->all(), [
           'old_password' => 'required',
           'new_password' => 'required|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid field',
                'errors' => $validator->errors(),
            ], 401);
        }

        if (app('hash')->check($request->old_password, $user->password)) {

            $new_password = app('hash')->make($request->new_password);
            $user->update([
                'password' => $new_password,
            ]);

            return response()->json([
               'message' => 'Password has been changed',
            ], 200);

        } else {
            return response()->json([
                'message' => 'Invalid field',
                'errors' => [
                    'old_password' => ['Old password is wrong'],
                ]
            ], 401);
        }
    }

    public function reset_password (Request $request) {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid field',
                'errors' => $validator->errors(),
            ], 401);
        }

        $user = $this->user->find($request->user_id);

        if (!$user) {
            return response()->json([
                'message' => 'User not found',
            ], 401);
        }

        $new_password = app('hash')->make($request->new_password);
        $user->update([
            'password' => $new_password,
        ]);

        return response()->json([
            'message' => 'Password has been reset',
        ], 200);

    }

}
