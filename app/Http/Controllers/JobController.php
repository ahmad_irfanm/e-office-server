<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Job;
use App\Track;
use App\User;
use Illuminate\Http\Request;
use Validator;

class JobController extends Controller
{
    //
    function __construct (Job $job, Track $track, User $user) {
        $this->job = $job;
        $this->track = $track;
        $this->user = $user;
    }

    public function index ($year) {
        $jobs = $this->job->where('date', 'LIKE', '%' . $year . '%')->with(['pic1' => function ($pic) {
            return $pic->with(['employee'])->get();
        }, 'pic2' => function ($pic) {
            return $pic->with(['employee'])->get();
        }])->orderBy('date', 'desc')->get();

        return response()->json([
            'message' => 'Data has been loaded',
            'values' => $jobs,
        ], 200);
    }

    public function get_by_status ($status) {
        $jobs = $this->job->where('status', $status)->orderBy('date', 'desc')->get();

        return response()->json([
            'message' => 'Data has been loaded',
            'values' => $jobs,
        ], 200);
    }

    public function show ($id) {
        $job = $this->job->where('id', $id)->with(['pic1' => function ($pic) {
            return $pic->with(['employee'])->get();
        }])->first();

        $last_activity = $this->track->where('pic', $job->pic1)->with(['pic' => function ($pic) {
            return $pic->with(['employee'])->get();
        }])->orderBy('id', 'desc')->get();

        return response()->json([
            'message' => 'Data has been loaded',
            'values' => [
                'job' => $job,
                'last_activity' => $last_activity,
            ],
        ], 200);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
           'title' => 'required',
           'description' => 'required',
           'pic1' => 'required',
           'pic2' => 'required',
           'date' => 'required',
           'time' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid field',
                'errors' => $validator->errors(),
            ], 401);

        $item = $this->job->create([
            'id' => $this->job->orderBy('id', 'desc')->first()->id + 1,
            'title' => ($request->customer ? '[' . $request->customer . '] ' : '') . $request->title,
            'description' => $request->description,
            'id_position' => $this->user->where('id', $request->pic1)->first()->employee->id_jabatan,
            'pic1' => $request->pic1,
            'pic2' => $request->pic2,
            'date' => date('d-m-Y', strtotime($request->date)) . ' ' . $request->time,
            'status' => 1
        ]);

        return response()->json([
            'message' => 'Job created successful',
            'values' => $item,
        ], 200);

    }

    public function destroy ($id) {
        $item = $this->job->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Job deleted successful',
        ], 200);
    }
}
