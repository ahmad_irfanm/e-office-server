<?php
namespace App\Http\Controllers;
use App\Employee;
use Illuminate\Http\Request;
use Validator;

class RequestController extends Controller
{
    //
    function __construct (\App\Request $item) {
        $this->item = $item;
    }

    public function index (Request $request) {
        $items = $this->item;

        if ($request->date && $request->date != "false") {
            $items = $items->where('date', $request->date);
        } else {
            $request->date = date('Y-m-d');
            $items = $items->where('date', $request->date);
        }

        if ($request->type == 'messenger') {
            $items = $items->where('transport', 'motor')->with(['messenger'])->orderBy('id', 'DESC')->get();
        } else {
            $items = $items->where('transport', 'mobil')->with(['driver'])->orderBy('id', 'DESC')->get();
        }

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => [
                'items' => $items,
                'date' => $request->date
            ]
        ], 200);
    }

    public function store (Request $request) {

        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'originator' => 'required',
            'priority' => 'required',
            'tujuan' => 'required',
            'waktu_datang' => 'required',
            'waktu_keluar' => 'required',
            'transport' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        // generate ID
        $last = $this->item->orderBy('id', 'desc')->first();
        $id = $last ? $last->id + 1 : 1;

        $create = $this->item->create([
            'id' => $id,
            'date' => $request->date,
            'id_driver' => 0,
            'jenis' => 0,
            'keterangan' => $request->keterangan ? $request->keterangan : '-',
            'originator' => $request->originator,
            'priority' => $request->priority,
            'status' => 2,
            'transport' => $request->transport,
            'tujuan' => $request->tujuan,
            'waktu_datang' => $request->waktu_datang,
            'waktu_keluar' => $request->waktu_keluar,
        ]);

        if ($create) {
            return response()->json([
                'message' => 'Request Created Successful',
                'values' => $create
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }

    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'originator' => 'required',
            'priority' => 'required',
            'tujuan' => 'required',
            'waktu_datang' => 'required',
            'waktu_keluar' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        $update = $item->update([
            'date' => $request->date,
            'id_driver' => $request->id_driver,
            'jenis' => $request->jenis,
            'keterangan' => $request->keterangan ? $request->keterangan : '-',
            'priority' => $request->priority,
            'status' => $request->status,
            'tujuan' => $request->tujuan,
            'waktu_datang' => $request->waktu_datang,
            'waktu_keluar' => $request->waktu_keluar,
        ]);

        if ($update) {
            return response()->json([
                'message' => 'Request Updated Successful',
                'values' => $item
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }

    }

    public function finish (Request $request, $id) {
        $item = $this->item->find($id);

        $update = $item->update([
            'status' => 0,
        ]);

        if ($update) {
            return response()->json([
                'message' => 'Request Updated Successful',
                'values' => $item
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }

    }

    public function get_all_drivers (Employee $employee) {
        $id_jabatan = 6;
        $drivers = $employee->where('id_jabatan', $id_jabatan)->orderBy('name', 'asc')->get();

        return response()->json([
            'message' => 'Drivers loaded successful',
            'values' => $drivers
        ], 200);
    }

    public function get_all_messengers (Employee $employee) {
        $id_jabatan = 5;
        $drivers = $employee->where('id_jabatan', $id_jabatan)->orderBy('name', 'asc')->get();

        return response()->json([
            'message' => 'Messenger loaded successful',
            'values' => $drivers
        ], 200);
    }
}
