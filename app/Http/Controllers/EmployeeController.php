<?php
namespace App\Http\Controllers;
use App\Employee;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Validator;

class EmployeeController extends Controller
{
    //
    function __construct (Employee $employee) {
        $this->employee = $employee;
    }

    public function index () {
        $items = $this->employee->orderBy('id_jabatan', 'asc')->orderBy('nik', 'asc')->with(['position' => function ($position) {
            $position->where('id', '>', '0')->get();
        }, 'user' => function ($user) {
            $user->where('active', '1')->get();
        }])->get()->filter(function ($employee) {
            return $employee->user && $employee->position;
        })->values()->all();

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $items
        ], 200);
    }

    public function show ($id) {
        $item = $this
            ->employee
            ->where('nik', $id)
            ->with(['user', 'educations' => function ($education) {
                $education->orderBy('period', 'DESC')->get();
            }, 'experiences', 'certificates', 'families', 'emergencies', 'position', 'maritals'])->first();

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $item
        ], 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'nik' => 'required|unique:tbl_employee,nik',
            'id_jabatan' => 'required',
            'place_birth' => 'required',
            'date_birth' => 'required|date_format:d-m-Y',
            'sex' => 'required',
            'foto' => 'file|mimes:png,jpg,jpeg,gif',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $file = $request->file('foto');

        // genrate username
        $username = strtolower(explode(' ', $request->name)[0]);

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/employees/';
            $file_name = $username . '_' . $request->nik . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = '-';
        }

        $item = $this->employee->create([
            'nik' => $request->nik,
            'id_jabatan' => $request->id_jabatan,
            'name' => $request->name,
            'id_card_type' => 'KTP',
            'id_card_number' => $request->id_card_number ? $request->id_card_number : '-',
            'address' => $request->address ? $request->address : '-',
            'place_birth' => $request->place_birth,
            'date_birth' => $request->date_birth,
            'sex' => $request->sex,
            'postal_code' => $request->postal_code ? $request->postal_code : '-',
            'city' => $request->city ? $request->city : '-',
            'mobile1' => $request->phone ? $request->phone : '-',
            'email' => $request->email ? $request->email : '-',
            'foto' => $file,
        ]);

        // generate ID user
        $last = User::orderBy('id', 'desc')->first();
        $id = $last ? $last->id + 1 : 1;

        // create user
        $user = User::create([
            'id' => $id,
            'nik' => $request->nik,
            'username' => $username,
            'password' => app('hash')->make($username),
            'active' => 1,
            'access' => 4,
            'is_login' => 0,
            'picture' => '-',
        ]);

        $item->user = $user;

        return response()->json([
            'message' => 'Employee created successful',
            'values' => $item,
        ], 200);
    }

    public function update (Request $request, $id) {
        $item = $this->employee->find($id);

        if (!$item) {
            return response()->json([
                'message' => 'Employee not found',
            ], 401);
        }

        $rules = [
            'name' => 'required',
            'nik' => 'required',
            'id_jabatan' => 'required',
            'place_birth' => 'required',
            'date_birth' => 'required|date_format:d-m-Y',
            'sex' => 'required',
            'foto' => 'file|mimes:png,jpg,jpeg,gif',
        ];

        if ($request->nik != $item->nik) {
            $rules['nik'] .= '|unique:tbl_employee,nik';
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $file = $request->file('foto');

        // genrate username
        $username = strtolower(explode(' ', $request->name)[0]);

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/employees/';
            $file_name = $username . '_' . $request->nik . '.' . $ext;

            unlink($destination_path . $item->foto);

            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $item->foto;
        }

        $item->update([
            'nik' => $request->nik,
            'id_jabatan' => $request->id_jabatan,
            'name' => $request->name,
            'id_card_type' => 'KTP',
            'id_card_number' => $request->id_card_number ? $request->id_card_number : '-',
            'address' => $request->address ? $request->address : '-',
            'place_birth' => $request->place_birth,
            'date_birth' => $request->date_birth,
            'sex' => $request->sex,
            'postal_code' => $request->postal_code ? $request->postal_code : '-',
            'city' => $request->city ? $request->city : '-',
            'mobile1' => $request->phone ? $request->phone : '-',
            'email' => $request->email ? $request->email : '-',
            'foto' => $file,
        ]);

        return response()->json([
            'message' => 'Employee updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->employee->find($id);

        if (!$item) {
            return response()->json([
                'message' => 'Employee not found',
            ], 401);
        }

        if ($item->foto != '-' && $item) {
            $destination_path = './upload/employees/';
            unlink($destination_path . $item->foto);
        }

        $item->user()->delete();
        $item->delete();

        return response()->json([
            'message' => 'Employee has been deleted'
        ], 200);
    }
}
