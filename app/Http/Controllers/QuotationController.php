<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Quotation;
use Illuminate\Http\Request;
use Validator;
class QuotationController extends Controller
{
    //
    function __construct(Quotation $quotation) {
        $this->quotation = $quotation;
    }

    public function index (Request $request) {
        $quotations = $this->quotation->orderBy('id', 'desc');

        if ($request->year) {
            $quotations = $quotations->whereYear('tanggal', $request->year);
        }

        $quotations = $quotations->get()->map(function ($i) {
            // change date format
            $i->tanggal = date('d/m/Y', strtotime($i->tanggal));
            return $i;
        });

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $quotations
        ], 200);
    }

    public function show ($id) {
        $quotation = $this->quotation->where('id', $id)->first();

        // change date format
        $quotation->tanggal_display = date('d/m/Y', strtotime($quotation->tanggal));

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $quotation
        ], 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'nomor' => 'required',
            'tanggal' => 'required',
            'sales' => 'required',
            'customer' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);


        $create = $this->quotation->create([
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'sales' => $request->sales,
            'customer' => $request->customer,
            'deskripsi' => $request->deskripsi,
            'file' => 'image.jpg'
        ]);

        if ($create) {
            return response()->json([
                'message' => 'Quotation Created Successful',
                'values' => $create
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }


    }

    public function destroy ($id) {
        $item = $this->quotation->find($id);

        $delete = $item->delete();

        if ($delete) {
            return response()->json([
                'message' => 'Quotation Deleted Successful'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function update (Request $request, $id) {
        $item = $this->quotation->find($id);

        $validator = Validator::make($request->all(), [
            'nomor' => 'required',
            'tanggal' => 'required',
            'sales' => 'required',
            'customer' => 'required',
            'deskripsi' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);


        $update = $item->update([
            'nomor' => $request->nomor,
            'tanggal' => $request->tanggal,
            'sales' => $request->sales,
            'customer' => $request->customer,
            'deskripsi' => $request->deskripsi,
            'file' => 'image.jpg'
        ]);

        if ($update) {
            return response()->json([
                'message' => 'Quotation Update Successful',
                'values' => $item
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

}
