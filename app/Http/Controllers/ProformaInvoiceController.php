<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\ProformaInvoice;
use Illuminate\Http\Request;
use Validator;
class ProformaInvoiceController extends Controller
{
    //
    function __construct(ProformaInvoice $pi) {
        $this->pi = $pi;
    }

    public function index (Request $request) {
        $pis = $this->pi->orderBy('id', 'desc');

        if ($request->year) {
            $pis = $pis->whereYear('tanggal', $request->year);
        }

        $pis = $pis->get()->map(function ($i) {

            // change format date
            $i->date_due = date('d/m/Y', strtotime($i->date_due));
            $i->date_issued = date('d/m/Y', strtotime($i->date_issued));
            $i->do_date = date('d/m/Y', strtotime($i->do_date));
            $i->invoice_date = date('d/m/Y', strtotime($i->invoice_date));
            $i->tanggal = date('d/m/Y', strtotime($i->tanggal));
            $i->po_date = date('d/m/Y', strtotime($i->po_date));

            return $i;
        });

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $pis
        ], 200);
    }

    public function show ($id) {
        $pi = $this->pi->where('id', $id)->first();

        // change format date
        $pi->date_due_display = date('d/m/Y', strtotime($pi->date_due));
        $pi->date_issued_display = date('d/m/Y', strtotime($pi->date_issued));
        $pi->do_date_display = date('d/m/Y', strtotime($pi->do_date));
        $pi->invoice_date_display = date('d/m/Y', strtotime($pi->invoice_date));
        $pi->tanggal_display = date('d/m/Y', strtotime($pi->tanggal));
        $pi->po_date_display = date('d/m/Y', strtotime($pi->po_date));

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $pi
        ], 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'customer' => 'required',
//            'date_due' => 'required',
            'date_issued' => 'required',
//            'do_date' => 'required',
            'do_number' => 'required',
            'invoice' => 'required',
            'invoice_date' => 'required',
            'invoice_number' => 'required',
            'ket_penagihan' => 'required',
            'keterangan' => 'required',
            'matauang_invoice' => 'required',
            'matauang_pajak' => 'required',
            'matauang_total_tagihan' => 'required',
            'nilai_total_tagihan' => 'required',
            'no_po_kontrak' => 'required',
            'pajak' => 'required',
            'rekening_pembayaran' => 'required',
            'tanggal' => 'required',
            'no_npwp_customer' => 'required',
            'po_date' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/proforma-invoices/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = 'null';
        }

        // generate ID
        $last = $this->pi->orderBy('id', 'desc')->first();
        $id = $last ? $last->id + 1 : 1;

        $create = $this->pi->create([
            'id' => $id,
            'customer' => $request->customer,
            'customer_id' => $request->customer_id,
            'date_due' => $request->date_due,
            'date_issued' => $request->date_issued,
            'do_date' => $request->do_date,
            'do_number' => $request->do_number,
            'invoice' => $request->invoice,
            'invoice_date' => $request->invoice_date,
            'invoice_number' => $request->invoice_number,
            'ket_penagihan' => $request->ket_penagihan,
            'keterangan' => $request->keterangan,
            'matauang_invoice' => $request->matauang_invoice,
            'matauang_pajak' => $request->matauang_pajak,
            'matauang_total_tagihan' => $request->matauang_total_tagihan,
            'nilai_total_tagihan' => $request->nilai_total_tagihan,
            'no_po_kontrak' => $request->no_po_kontrak,
            'pajak' => $request->pajak,
            'rekening_pembayaran' => $request->rekening_pembayaran,
            'spp_number' => '-',
            'tanggal' => $request->tanggal,
            'no_npwp_customer' => $request->no_npwp_customer,
            'po_date' => $request->po_date,
            'file' => $file,
            'additional' => $request->additional,
            'detail_goods' => $request->detail_goods,
        ]);

        if ($create) {
            return response()->json([
                'message' => 'Proforma Invoice Created Successful',
                'values' => $create
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function upload (Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:pdf,png,docx',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);
        }

        $file = $request->file('file');

        $original_filename = $file->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $ext = end($original_filename_arr);
        $destination_path = './upload/proforma/';
        $file_name = 'proforma_' . $id . '.' . $ext;
        $upload_file = $file->move($destination_path, $file_name);
        $file = $file_name;

        $this->pi->find($id)->update([
            'file' => $file,
        ]);

        // check upload file sop
        if (!$upload_file) {
            return response()->json([
                'msg' => 'upload file error!'
            ], 401);
        }

        return response()->json([
            'message' => 'File uploaded successful',
        ], 200);



    }

    public function update (Request $request, $id) {
        $pi = $this->pi->find($id);

        $validator = Validator::make($request->all(), [
            'customer' => 'required',
//            'date_due' => 'required',
            'date_issued' => 'required',
//            'do_date' => 'required',
            'do_number' => 'required',
            'invoice' => 'required',
            'invoice_date' => 'required',
            'invoice_number' => 'required',
            'ket_penagihan' => 'required',
            'keterangan' => 'required',
            'matauang_invoice' => 'required',
            'matauang_pajak' => 'required',
            'matauang_total_tagihan' => 'required',
            'nilai_total_tagihan' => 'required',
            'no_po_kontrak' => 'required',
            'pajak' => 'required',
            'rekening_pembayaran' => 'required',
            'tanggal' => 'required',
            'no_npwp_customer' => 'required',
            'po_date' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);


        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/proforma-invoices/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $pi->file;
        }

        $update = $pi->update([
            'customer' => $request->customer,
            'customer_id' => $request->customer_id,
            'date_due' => $request->date_due,
            'date_issued' => $request->date_issued,
            'do_date' => $request->do_date,
            'do_number' => $request->do_number,
            'invoice' => $request->invoice,
            'invoice_date' => $request->invoice_date,
            'invoice_number' => $request->invoice_number,
            'ket_penagihan' => $request->ket_penagihan,
            'keterangan' => $request->keterangan,
            'matauang_invoice' => $request->matauang_invoice,
            'matauang_pajak' => $request->matauang_pajak,
            'matauang_total_tagihan' => $request->matauang_total_tagihan,
            'nilai_total_tagihan' => $request->nilai_total_tagihan,
            'no_po_kontrak' => $request->no_po_kontrak,
            'pajak' => $request->pajak,
            'rekening_pembayaran' => $request->rekening_pembayaran,
            'spp_number' => '-',
            'tanggal' => $request->tanggal,
            'no_npwp_customer' => $request->no_npwp_customer,
            'po_date' => $request->po_date,
            'file' => $file,
            'additional' => $request->additional,
            'detail_goods' => $request->detail_goods,
        ]);

        if ($update) {
            return response()->json([
                'message' => 'Proforma Invoice Updated Successful',
                'values' => $pi
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function destroy ($id) {
        $item = $this->pi->find($id);

        $delete = $item->delete();

        if ($delete) {
            return response()->json([
                'message' => 'Proforma Invoice Deleted Successful'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

}
