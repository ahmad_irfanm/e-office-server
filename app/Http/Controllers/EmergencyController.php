<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class EmergencyController extends Controller
{
    //
    public function __construct(\App\Emergency $item)
    {
        //
        $this->item = $item;
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'name' => 'required',
            'relation' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id_user' => $request->id_user,
            'name' => $request->name,
            'relation' => $request->relation,
            'phone' => $request->phone,
            'address' => $request->address,
        ]);

        return response()->json([
            'message' => 'Emergency created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'name' => 'required',
            'relation' => 'required',
            'phone' => 'required',
            'address' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'name' => $request->name,
            'relation' => $request->relation,
            'phone' => $request->phone,
            'address' => $request->address,
        ]);

        return response()->json([
            'message' => 'Emergency updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Emergency deleted successful',
        ], 200);
    }
}
