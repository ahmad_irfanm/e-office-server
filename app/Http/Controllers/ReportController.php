<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Report;
use App\TodoList;
use Illuminate\Http\Request;
class ReportController extends Controller
{
    //
    function __construct(Report $report, TodoList $todoList)
    {
        $this->report = $report;
        $this->todoList = $todoList;
    }

    public function index ($date_start, $date_end) {

        $todos = $this->todoList
            ->where('id_user', auth()->user()->id)
            ->get()
            ->map(function ($todo) { return $todo->id; });

        $reports = $this->report
            ->with(['todolist'])
            ->whereBetween('date', [$date_start, $date_end])
            ->whereIn('id_todo', $todos)
            ->get();

        return response()->json([
            'message' => 'Data has been load',
            'values' => $reports,
        ], 200);
    }
}
