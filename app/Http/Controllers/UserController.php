<?php
namespace App\Http\Controllers;
use App\Employee;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
class UserController extends Controller
{
    //
    function __construct (User $user, Employee $employee) {
        $this->item = $user;
        $this->employee = $employee;
    }

    public function index (Request $request) {
        if ($request->id_jabatan) {
            $employees = $this->employee->where('id_jabatan', $request->id_jabatan)->orderBy('name', 'asc')->get()->filter(function ($emp) {
                return $emp->user()->where('active', 1)->first();
            })->values();
            return $employees;
        }
    }
}
