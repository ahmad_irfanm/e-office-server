<?php
namespace App\Http\Controllers;
use App\ChargedUnit;
use Illuminate\Http\Request;
use Validator;

class ChargedUnitController extends Controller
{
    //
    public function __construct(ChargedUnit $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $year = $request->year ? $request->year : date('Y');

        $items = $this->item
            ->where('kuartal', $request->kuartal)
            ->whereYear('timestamp', $year)
            ->get()->map(function ($item) {
                return [
                    'month' => date('F', strtotime($item->timestamp)),
                    'charged_units' => $item->charged_units,
                ];
            });

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:csv',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        // Upload Here

        return response()->json([
            'message' => 'Charged Unit created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:csv',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        // Upload Here

        return response()->json([
            'message' => 'Charged Unit updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Charged Unit deleted successful',
        ], 200);
    }
}
