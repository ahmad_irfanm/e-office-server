<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;

class EducationController extends Controller
{
    //
    public function __construct(\App\Education $item)
    {
        //
        $this->item = $item;
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'school_name' => 'required',
            'period' => 'required',
            'title' => 'required',
            'department' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id_user' => $request->id_user,
            'school_name' => $request->school_name,
            'period' => $request->period,
            'title' => $request->title,
            'department' => $request->department,
            'description' => $request->description,
        ]);

        return response()->json([
            'message' => 'Education created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'school_name' => 'required',
            'period' => 'required',
            'title' => 'required',
            'department' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'school_name' => $request->school_name,
            'period' => $request->period,
            'title' => $request->title,
            'department' => $request->department,
            'description' => $request->description,
        ]);

        return response()->json([
            'message' => 'Education updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Education deleted successful',
        ], 200);
    }

}
