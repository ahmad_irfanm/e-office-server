<?php
namespace App\Http\Controllers;
use Anam\PhantomMagick\Converter;
use App\Http\Controllers\Controller;
use App\InvoiceCustomer;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Spatie\Browsershot\Browsershot;
use Validator;

class InvoiceCustomerController extends Controller
{
    //
    function __construct (InvoiceCustomer $ic) {
        $this->ic = $ic;
    }

    public function index (Request $request) {
        $ics = $this->ic->orderBy('id', 'desc');

        if ($request->year) {
            $ics = $ics->whereYear('tanggal', $request->year);
        }

        $ics = $ics->get()->map(function ($i) {

            // change format date
            $i->date_issued = $i->date_issued ? date('d/m/Y', strtotime($i->date_issued)) : NULL;
            $i->do_date = $i->do_date ? date('d/m/Y', strtotime($i->do_date)) : NULL;
            $i->invoice_date = $i->invoice_date ? date('d/m/Y', strtotime($i->invoice_date)) : NULL;
            $i->tanggal = $i->tanggal ? date('d/m/Y', strtotime($i->tanggal)) : NULL;
            $i->tgl_faktur_pajak = $i->tgl_faktur_pajak ? date('d/m/Y', strtotime($i->tgl_faktur_pajak)) : NULL;
            $i->po_date = $i->po_date ? date('d/m/Y', strtotime($i->po_date)) : NULL;

            return $i;

        });;

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $ics
        ], 200);
    }

    public function upload (Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:pdf,png,docx',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);
        }

        $file = $request->file('file');

        $original_filename = $file->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $ext = end($original_filename_arr);
        $destination_path = './upload/invoice-customers/';
        $file_name = 'invoice_' . $id . '.' . $ext;
        $upload_file = $file->move($destination_path, $file_name);
        $file = $file_name;

        $this->ic->find($id)->update([
            'file' => $file,
        ]);

        // check upload file sop
        if (!$upload_file) {
            return response()->json([
                'msg' => 'upload file error!'
            ], 401);
        }

        return response()->json([
            'message' => 'File uploaded successful',
        ], 200);



    }

    public function download_invoice ($id) {
        $conv = new Converter();
        $conv->addPage('<html><body><h1>Hello</h1></body></html>')
            ->toPdf()
            ->download('download.pdf');
    }

    public function print_invoice ($id) {
        $item = $this->ic->find($id);

        $item->goods = $item->detail_goods ? json_decode($item->detail_goods) : [];


        $total = 0;
        foreach ($item->goods as $goods) {

            $total += $goods->total;

        }

        $item->subtotal = $total;
        $item->terbilang = $this->terbilang($total);
        $item->vat = $total * 0.1;
        $item->total = $total + $item->vat;

        $rek = [
          'Bank Mandiri' => ['102-00-0442520-0', '102-00-0440635-8'],
          'Bank BRI' => ['176702000001308', '176701000015301'],
        ];

        $item->rek_usd = "-";
        $item->rek_rupiah = "-";

        if (isset($rek[$item->rekening_pembayaran][0])) {
            $item->rek_usd = $rek[$item->rekening_pembayaran][0];
            $item->rek_rupiah = $rek[$item->rekening_pembayaran][1];
        }



//        return $item;

        if ($item->customer_data) {
            $item->additional = $item->additional ? json_decode($item->additional) : [];


            if ($item->customer_data->id == 13) {
                $pdf = PDF::loadView('invoice-customer.telkom', ['item' => $item])->setPaper('a4');
                $filename = str_replace('/', '-', $item->invoice_number);
                return $pdf->download('invoice-' . $filename . '.pdf');
            } elseif ($item->customer_data->id == 4) {
//                return view('invoice-customer.indosat', [
//                    'item' => $item,
//                ]);

                $pdf = PDF::loadView('invoice-customer.indosat', ['item' => $item])->setPaper('a4');
                $filename = str_replace('/', '-', $item->invoice_number);
                return $pdf->download('invoice-' . $filename . '.pdf');
            }


        }

//         return view('invoice-customer.all', [
//             'item' => $item,
//         ]);

        $pdf = PDF::loadView('invoice-customer.all', ['item' => $item])->setPaper('a4');
       $filename = str_replace('/', '-', $item->invoice_number);
        return $pdf->download('invoice-' . $filename . '.pdf');
    }

    public function penyebut ($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }
        return $temp;
    }

    public function terbilang ($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }
        return $hasil;
    }

    // Old
    public function print_invoice_ ($id) {

        $item = $this->ic->find($id);

        $path = 'template/invoice-all.rtf';
        $file =  rtrim(app()->basePath('public/' . $path), '/');

        $detail_goods = [];
        $sub_total = 0;
        $amount = "";
        $price = "";
        $total = "";
        $goods = "";

        if ($item->detail_goods) {
            $detail_goods = json_decode($item->detail_goods);
        }

        foreach ($detail_goods as $detail_good) {
            $sub_total += $detail_good->total;
            $amount .= $detail_good->amount . " \n";
            $price .= $detail_good->price . " \n";
            $total .= $detail_good->total . " \n";
            if (isset($detail_good->name)) {
                $goods .= $detail_good->name . " \n";
            } else {
                $goods = "- \n";
            }
        }

        $vat = 0.1 * $sub_total;
        $grand_total = $sub_total + $vat;

        $array = array(
            '[CUSTOMER]' => $item->customer,
            '[ADDRESS]' => $item->customer_data ? $item->customer_data->address : '-',
            '[GOODS]' => $goods,
            '[AMOUNT]' => $amount,
            '[PRICE]' => $price,
            '[TOTAL]' => $total,
            '[SUBTOTAL]' => $sub_total,
            '[VAT]' => $vat,
            '[GRANDTOTAL]' => $grand_total,
        );

        $nama_file = 'invoice.doc';

        $wordtemplate = new \Novay\WordTemplate\WordTemplate();
        return $wordtemplate->export($file, $array, $nama_file);
    }

    public function show ($id) {
        $ic = $this->ic->find($id);

        // change format date
        $ic->date_issued_display = date('d/m/Y', strtotime($ic->date_issued));
        $ic->do_date_display = date('d/m/Y', strtotime($ic->do_date));
        $ic->invoice_date_display = date('d/m/Y', strtotime($ic->invoice_date));
        $ic->tanggal_display = date('d/m/Y', strtotime($ic->tanggal));
        $ic->tgl_faktur_pajak_display = date('d/m/Y', strtotime($ic->tgl_faktur_pajak));
        $ic->po_date_display = date('d/m/Y', strtotime($ic->po_date));

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $ic
        ]);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
                'customer' => 'required',
//                'date_due' => 'required',
                'date_issued' => 'required',
//                'do_date' => 'required',
                'po_date' => 'required',
                'do_number' => 'required',
                'invoice' => 'required',
                'invoice_date' => 'required',
                'invoice_number' => 'required',
                'ket_penagihan' => 'required',
                'keterangan' => 'required',
                'kwitansi_number' => 'required',
                'matauang_invoice' => 'required',
                'no_faktur_pajak' => 'required',
                'no_npwp_customer' => 'required',
                'no_po_kontrak' => 'required',
                'rekening_pembayaran' => 'required',
                'spp_number' => 'required',
                'tanggal' => 'required',
                'tgl_faktur_pajak' => 'required',
        ]);

        // automatic calculation of PPN
        $request->matauang_pajak = $request->matauang_invoice;
        $request->pajak = 0.1 * $request->invoice;

        // automatic calculation of nilai tagihan
        $request->matauang_total_tagihan = $request->matauang_pajak;
        $request->nilai_total_tagihan = $request->invoice + $request->pajak;

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);


        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/invoice-customers/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = null;
        }

        // generate ID
        $last = $this->ic->orderBy('id', 'desc')->first();
        $id = $last ? $last->id + 1 : 1;

        $create = $this->ic->create([
            'id' => $id,
            'customer' => $request->customer,
            'customer_id' => $request->customer_id,
            'date_due' => $request->date_due,
            'date_issued' => $request->date_issued,
            'do_date' => $request->do_date,
            'do_number' => $request->do_number,
            'invoice' => $request->invoice,
            'invoice_date' => $request->invoice_date,
            'invoice_number' => $request->invoice_number,
            'ket_penagihan' => $request->ket_penagihan,
            'keterangan' => $request->keterangan,
            'kwitansi_number' => $request->kwitansi_number,
            'matauang_invoice' => $request->matauang_invoice,
            'matauang_pajak' => $request->matauang_pajak,
            'matauang_total_tagihan' => $request->matauang_total_tagihan,
            'nilai_total_tagihan' => $request->nilai_total_tagihan,
            'no_faktur_pajak' => $request->no_faktur_pajak,
            'no_npwp_customer' => $request->no_npwp_customer,
            'no_po_kontrak' => $request->no_po_kontrak,
            'pajak' => $request->pajak,
            'rekening_pembayaran' => $request->rekening_pembayaran,
            'spp_number' => $request->spp_number,
            'tanggal' => $request->tanggal,
            'tgl_faktur_pajak' => $request->tgl_faktur_pajak,
            'po_date' => $request->po_date,
            'file' => $file,
            'additional' => $request->additional,
            'detail_goods' => $request->detail_goods,

            'nilai_pajak' => 0,
            'kurs_pajak' => 0
        ]);

        if ($create) {
            return response()->json([
                'message' => 'Invoice Customer Created Successful',
                'values' => $create
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function update (Request $request, $id) {
        $ic = $this->ic->find($id);

        $validator = Validator::make($request->all(), [
            'customer' => 'required',
//            'date_due' => 'required',
            'date_issued' => 'required',
//            'do_date' => 'required',
            'po_date' => 'required',
            'do_number' => 'required',
            'invoice' => 'required',
            'invoice_date' => 'required',
            'invoice_number' => 'required',
            'ket_penagihan' => 'required',
            'keterangan' => 'required',
            'kwitansi_number' => 'required',
            'matauang_invoice' => 'required',
            'no_faktur_pajak' => 'required',
            'no_npwp_customer' => 'required',
            'no_po_kontrak' => 'required',
            'rekening_pembayaran' => 'required',
            'spp_number' => 'required',
            'tanggal' => 'required',
            'tgl_faktur_pajak' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/invoice-customers/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $ic->file;
        }


        $update = $ic->update([
            'customer' => $request->customer,
            'customer_id' => $request->customer_id,
            'date_due' => $request->date_due,
            'date_issued' => $request->date_issued,
            'do_date' => $request->do_date,
            'do_number' => $request->do_number,
            'invoice' => $request->invoice,
            'invoice_date' => $request->invoice_date,
            'invoice_number' => $request->invoice_number,
            'ket_penagihan' => $request->ket_penagihan,
            'keterangan' => $request->keterangan,
            'kwitansi_number' => $request->kwitansi_number,
            'matauang_invoice' => $request->matauang_invoice,
            'matauang_pajak' => $request->matauang_pajak,
            'matauang_total_tagihan' => $request->matauang_total_tagihan,
            'nilai_total_tagihan' => $request->nilai_total_tagihan,
            'no_faktur_pajak' => $request->no_faktur_pajak,
            'no_npwp_customer' => $request->no_npwp_customer,
            'no_po_kontrak' => $request->no_po_kontrak,
            'pajak' => $request->pajak,
            'rekening_pembayaran' => $request->rekening_pembayaran,
            'spp_number' => $request->spp_number,
            'tanggal' => $request->tanggal,
            'tgl_faktur_pajak' => $request->tgl_faktur_pajak,
            'po_date' => $request->po_date,
            'file' => $file,
            'additional' => $request->additional,
            'detail_goods' => $request->detail_goods,

            'nilai_pajak' => 0,
            'kurs_pajak' => 0
        ]);

        if ($update) {
            return response()->json([
                'message' => 'Invoice Customer Updated Successful',
                'values' => $update
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

    public function destroy ($id) {
        $item = $this->ic->find($id);

        $delete = $item->delete();

        if ($delete) {
            return response()->json([
                'message' => 'Invoice Customer Deleted Successful'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }

}
