<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class ExperienceController extends Controller
{
    //
    public function __construct(\App\Experience $item)
    {
        //
        $this->item = $item;
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'id_user' => 'required',
            'company_name' => 'required',
            'position' => 'required',
            'location' => 'required',
            'period' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id_user' => $request->id_user,
            'company_name' => $request->company_name,
            'position' => $request->position,
            'location' => $request->location,
            'period' => $request->period,
            'description' => $request->description,
        ]);

        return response()->json([
            'message' => 'Experience created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'position' => 'required',
            'location' => 'required',
            'period' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'company_name' => $request->company_name,
            'position' => $request->position,
            'location' => $request->location,
            'period' => $request->period,
            'description' => $request->description,
        ]);

        return response()->json([
            'message' => 'Experience updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Experience deleted successful',
        ], 200);
    }
}
