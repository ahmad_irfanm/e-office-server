<?php
namespace App\Http\Controllers;
use App\Customer;
use Illuminate\Http\Request;
use Validator;

class CustomerController extends Controller
{
    //
    public function __construct(Customer $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $items = $this->item
            ->orderBy('name', 'asc')
            ->with(['customer_contacts'])
            ->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item = $this->item->create([
            'name' => $request->name,
            'address' => $request->address,
            'npwp' => $request->npwp,
        ]);

        return response()->json([
            'message' => 'Customer created successful',
            'values' => $item,
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->where('id', $id)->with(['customer_contacts'])->first();

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'name' => $request->name,
            'address' => $request->address,
            'npwp' => $request->npwp,
        ]);

        return response()->json([
            'message' => 'Customer updated successful',
            'values' => $item,
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Customer deleted successful',
        ], 200);
    }
}
