<?php
namespace App\Http\Controllers;
use App\Customer;
use App\CustomerContact;
use App\MarketingModels\Company;
use Illuminate\Http\Request;
use Validator;

class CustomerContactController extends Controller
{
    //
    public function __construct(CustomerContact $item, Customer $customer)
    {
        //
        $this->item = $item;
        $this->customer = $customer;
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        // define customer
        $customer = $this->customer->find($request->customer_id);
        if (!$customer)
            return response()->json([
                'message' => 'Customer not found',
            ], 401);

        $item = $this->item->create([
            'customer_id' => $request->customer_id,
            'name' => $request->name,
            'phone' => $request->phone,
            'position' => $request->position,
            'email' => $request->email,
        ]);

        return response()->json([
            'message' => 'Customer Contact created successful',
            'values' => $item,
        ], 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Customer Contact not found',
            ], 401);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'position' => $request->position,
            'email' => $request->email,
        ]);

        return response()->json([
            'message' => 'Customer Contact updated successful',
            'values' => $item,
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        if (!$item)
            return response()->json([
                'message' => 'Customer Contact not found',
            ], 401);

        $item->delete();

        return response()->json([
            'message' => 'Customer Contact deleted successful',
        ], 200);
    }

}
