<?php
namespace App\Http\Controllers;
use App\AbsentModels\Absent;
use App\AbsentModels\AttLog;
use \App\AbsentModels\Employee;
use App\AbsentModels\ExScan;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //
    function __construct (Employee $employee, Absent $absent, User $user, ExScan $exScan, AttLog $attLog) {
        $this->absent = $absent;
        $this->employee = $employee;
        $this->user = $user;
        $this->exScan = $exScan;
        $this->attLog = $attLog;
    }

    public function summary () {
        return response()->json("Nice to meet you. Have a nice day", 200);
    }

    public function punctuality_now ($type) {
        if ($type == 'year') {
            $att_logs = $this->attLog->whereYear('scan_date', date('Y'))->get();
        } elseif ($type == 'month') {
            $att_logs = $this->attLog->whereMonth('scan_date', date('m'))->whereYear('scan_date', date('Y'))->get();
        } elseif ($type == 'week') {
            $att_logs = $this->attLog->whereBetween('scan_date', [date('Y-m-d', strtotime('-7 days', strtotime(date('Y-m-d')))), date('Y-m-d')])->get();
        }

        // optimize scans
        $optimize = collect($this->optimize($att_logs));

        // define result
        $result = [];

        $result['month'] = date('F');
        $result['year'] = date('Y');

        // tepat waktu
        $result['labels'][] = "Tepat waktu";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['time_come'] <= '08:00:00';
        })->count();

        // telat
        $result['labels'][] = "Telat";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['time_come'] > '08:00:00' && !isset($scan['ex_scan']);
        })->count();

        // izin telat
        $result['labels'][] = "Izin telat";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 10 ? true : false) : false;
        })->count();

        // izin pulang cepat
        $result['labels'][] = "Izin pulang cepat";
        $result['series'][] = $optimize->filter(function ($scan) {
            return $scan['ex_scan'] ? ($scan['ex_scan']['scan_id'] == 11 ? true : false) : false;
        })->count();

        // resonse
        return $result;
    }

    public function optimize ($att_logs) {

        $exScan = $this->exScan;
        $duplicates = collect([]);

        $att_logs->each(function ($att_log) use ($duplicates) {
            $scan_date = date('Y-m-d', strtotime($att_log->scan_date));
            if (!$duplicates->contains($scan_date)) {
                $duplicates->push($scan_date);
            }
        });

        $chunk = $duplicates->map(function ($date) use ($att_logs, $exScan) {
            // define item
            $item = [];

            $atts = $att_logs->filter(function ($att_log) use ($date) {
                return date('Y-m-d', strtotime($att_log->scan_date)) == $date;
            });

            $item['date']= $date;

            $item['time_come'] = $atts->filter(function ($att) {
                return date('H:i:s', strtotime($att->scan_date)) < '17:00:00';
            })->sortBy('scan_date')->first();

            $item['time_leave'] = $atts->filter(function ($att) {
                return date('H:i:s', strtotime($att->scan_date)) > '17:00:00';
            })->sortByDesc('scan_date')->first();

            $item['duration'] = '-';

            if ($item['time_come'] && $item['time_leave']) {
                $create_time_come = date_create($item['time_come']->scan_date);
                $create_time_leave = date_create($item['time_leave']->scan_date);

                $diff = date_diff($create_time_come, $create_time_leave);
                $item['duration'] = $diff->h . ' jam ' . $diff->i . ' menit ' . $diff->s . ' detik.';
            }

            $item['time_come'] = $item['time_come'] ? date('H:i:s', strtotime($item['time_come']->scan_date)) : '-';
            $item['time_leave'] = $item['time_leave'] ? date('H:i:s', strtotime($item['time_leave']->scan_date)) : '-';

            // get ex scans
            $item['ex_scan'] = $exScan->where('emp_id_auto', 225)->with(['scan_status'])->whereDate('tgl_scan', $date)->orderBy('lastupdate_date', 'desc')->first();

            return $item;

        })->sortByDesc('date')->values()->all();

        return $chunk;
    }
}
