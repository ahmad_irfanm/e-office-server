<?php
namespace App\Http\Controllers;
use App\MonthlyReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;

class MonthlyReportController extends Controller
{
    //
    public function __construct(MonthlyReport $item)
    {
        //
        $this->item = $item;
    }

    public function index (Request $request) {
        $year = $request->year ? $request->year : date('Y');
        $quartal = [[1,2,3], [4,5,6], [7,8,9], [10, 11, 12]][$request->kuartal - 1];

        $start = Carbon::create($year, $quartal[0])->format('Y-m-d');
        $end = Carbon::create($year, $quartal[0])->addMonths(3)->format('Y-m-d');

        $items = $this->item
            ->whereBetween('date', [$start, $end])
            ->orderBy('date', 'desc')
            ->get();

        return response()->json($items, 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'activity' => 'required',
            'hutchison_pic' => 'required',
            'argasolusi_pic' => 'required',
            'start_date' => 'required',
            'completed_date' => 'required',
            'remark' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $this->item->create([
            'id' => $this->item->orderBy('id', 'desc')->first()->id + 1,
            'date' => $request->date,
            'activity' => $request->activity,
            'hutchison_pic' => $request->hutchison_pic,
            'argasolusi_pic' => $request->argasolusi_pic,
            'start_date' => $request->start_date,
            'completed_date' => $request->completed_date,
            'remark' => $request->remark,
        ]);


        return response()->json([
            'message' => 'Monthly Report created successful',
        ], 200);
    }

    public function show ($id) {
        $item = $this->item->find($id);

        return response()->json($item, 200);
    }

    public function update (Request $request, $id) {
        $item = $this->item->find($id);

        $validator = Validator::make($request->all(), [
            'date' => 'required',
            'activity' => 'required',
            'hutchison_pic' => 'required',
            'argasolusi_pic' => 'required',
            'start_date' => 'required',
            'completed_date' => 'required',
            'remark' => 'required',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);

        $item->update([
            'date' => $request->date,
            'activity' => $request->activity,
            'hutchison_pic' => $request->hutchison_pic,
            'argasolusi_pic' => $request->argasolusi_pic,
            'start_date' => $request->start_date,
            'completed_date' => $request->completed_date,
            'remark' => $request->remark,
        ]);

        return response()->json([
            'message' => 'Monthly Report updated successful',
        ], 200);
    }

    public function destroy ($id) {
        $item = $this->item->find($id);

        $item->delete();

        return response()->json([
            'message' => 'Monthly Report deleted successful',
        ], 200);
    }
}
