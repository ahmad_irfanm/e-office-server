<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Purchase;
use App\Specification;
use Illuminate\Http\Request;
use Validator;

class PurchaseController extends Controller
{
    //
    function __construct (Purchase $purchase, Specification $specification) {
        $this->purchase = $purchase;
        $this->specification = $specification;
    }

    public function index (Request $request) {
        $purchases = $this->purchase->orderBy('id', 'desc');

        if ($request->year) {
            $purchases = $purchases->whereYear('tanggal', $request->year);
        }

        $purchases = $purchases->get()->map(function ($i) {

            // change format date
            $i->tanggal = date('d/m/Y', strtotime($i->tanggal));

            return $i;

        });;

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $purchases
        ], 200);
    }

    public function show ($id) {
        $purchase = $this->purchase
            ->where('id', $id)
            ->with(['specifications'])
            ->first();

        // change format date
        $purchase->tanggal_display = date('d/m/Y', strtotime($purchase->tanggal));

        return response()->json([
            'message' => 'Data loaded successful',
            'values' => $purchase
        ], 200);
    }

    public function store (Request $request) {
        $validator = Validator::make($request->all(), [
            'currency_diskon' => 'required',
            'nama_customer' => 'required',
            'delivery_order' => 'required',
            'diskon' => 'required',
            'nama_proyek' => 'required',
            'nomor' => 'required',
            'no_kontrak' => 'required',
            'sn' => 'required',
            'tanggal' => 'required',
            'specifications' => 'array',
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/purchases/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = 'null';
        }

        // generate ID
        $last = $this->purchase->orderBy('id', 'desc')->first();
        $id = $last ? $last->id + 1 : 1;

        $create = $this->purchase->create([
            'id' => $id,
            'currency_diskon' => $request->currency_diskon,
            'nama_customer' => $request->nama_customer,
            'delivery_order' => $request->delivery_order,
            'diskon' => $request->diskon,
            'nama_proyek' => $request->nama_proyek,
            'nomor' => $request->nomor,
            'no_kontrak' => $request->no_kontrak,
            'sn' => $request->sn,
            'tanggal' => $request->tanggal,
            'file' => $file
        ]);

        if ($request->specifications) {
            foreach ($request->specifications as $specification) {
                $specification = $this->specification->create([
                    'id_purchase' => $create->id,
                    'name' => $specification['name'],
                    'qty' => $specification['qty'],
                    'currency' => $specification['currency'],
                    'price' => $specification['price'],
                    'total' => $specification['total']
                ]);
            }
        }

        if ($create) {
            return response()->json([
                'message' => 'Purchase Created Successful',
                'values' => $create
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }

    }

    public function upload (Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:pdf,png,docx',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors(),
            ], 401);
        }

        $file = $request->file('file');

        $original_filename = $file->getClientOriginalName();
        $original_filename_arr = explode('.', $original_filename);
        $ext = end($original_filename_arr);
        $destination_path = './upload/purchases/';
        $file_name = 'purchase_' . $id . '.' . $ext;
        $upload_file = $file->move($destination_path, $file_name);
        $file = $file_name;

        $this->purchase->find($id)->update([
            'file' => $file,
        ]);

        // check upload file sop
        if (!$upload_file) {
            return response()->json([
                'msg' => 'upload file error!'
            ], 401);
        }

        return response()->json([
            'message' => 'File uploaded successful',
        ], 200);

    }

    public function update (Request $request, $id) {
        $purchase = $this->purchase->find($id);

        $validator = Validator::make($request->all(), [
            'currency_diskon' => 'required',
            'nama_customer' => 'required',
            'delivery_order' => 'required',
            'diskon' => 'required',
            'nama_proyek' => 'required',
            'nomor' => 'required',
            'no_kontrak' => 'required',
            'sn' => 'required',
            'tanggal' => 'required'
        ]);

        if ($validator->fails())
            return response()->json([
                'message' => 'Invalid fields',
                'errors' => $validator->errors()
            ], 401);

        $file = $request->file('file');

        if ($file) {
            $original_filename = $file->getClientOriginalName();
            $original_filename_arr = explode('.', $original_filename);
            $ext = end($original_filename_arr);
            $destination_path = './upload/purchases/';
            $file_name = 'U-' . time() . '.' . $ext;
            $upload_file = $file->move($destination_path, $file_name);
            $file = $file_name;

            // check upload file sop
            if (!$upload_file) {
                return response()->json([
                    'msg' => 'upload file error!'
                ], 401);
            }
        } else {
            $file = $purchase->file;
        }

        $update = $purchase->update([
            'currency_diskon' => $request->currency_diskon,
            'nama_customer' => $request->nama_customer,
            'delivery_order' => $request->delivery_order,
            'diskon' => $request->diskon,
            'nama_proyek' => $request->nama_proyek,
            'nomor' => $request->nomor,
            'no_kontrak' => $request->no_kontrak,
            'sn' => $request->sn,
            'tanggal' => $request->tanggal,
            'file' => $file
        ]);

        if ($request->specifications) {
            foreach ($purchase->specifications as $specification) {
                $specification->delete();
            }

            foreach ($request->specifications as $specification) {
                $specification = $this->specification->create([
                    'id_purchase' => $purchase->id,
                    'name' => $specification['name'],
                    'qty' => $specification['qty'],
                    'currency' => $specification['currency'],
                    'price' => $specification['price'],
                    'total' => $specification['total']
                ]);
            }
        }

        if ($update) {
            return response()->json([
                'message' => 'Purchase Created Successful',
                'values' => $purchase
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }

    }

    public function destroy ($id) {
        $item = $this->purchase->find($id);

        $delete = $item->delete();

        if ($delete) {
            return response()->json([
                'message' => 'Purchase Order from Customer Deleted Successful'
            ], 200);
        } else {
            return response()->json([
                'message' => 'Data cannot be processed',
            ], 422);
        }
    }
}
