<?php

use Illuminate\Database\Seeder;

class InvoiceCustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (\App\InvoiceCustomer::all() as $item) {
            $item->update([
                'po_date' => $item->date_issued,
            ]);
        }
    }
}
