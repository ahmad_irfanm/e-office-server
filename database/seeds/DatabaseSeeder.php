<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        foreach (\App\MarketingModels\Forecast::all() as $forecast) {
            if (explode('UR', $forecast->total_price)[0] == 'E') {
                $total_price = explode('UR', $forecast->total_price)[1];

                $forecast->update([
                    'total_price' => 'EUR ' . $total_price,
                ]);
            }
        }

        exit;

        foreach (range(1, 100) as $i) {
            \App\Quotation::create([
                'nomor' => $faker->randomNumber(8),
                'tanggal' => $faker->date('Y-m-d'),
                'sales' => $faker->name,
                'customer' => $faker->name,
                'deskripsi' => $faker->randomLetter,
                'file' => 'image.jpg'
            ]);
        }

        exit;

        foreach (\App\User::all() as $user) {
            $user->update([
                'password' => app('hash')->make('indonesia'),
            ]);
        }

        exit;

        $niks = [NULL, 48511015, 27505002, 17605001, NULL, 17514023, 17905003, 28806006, 16715028, NULL, 2015090001, 28710012];
        foreach (\App\MarketingModels\User::all() as $i => $item) {
            $item->update([
               "nik" => $niks[$i],
            ]);
        }

        exit;


        foreach (\App\ProformaInvoice::all() as $item) {
            $item->update([
                'po_date' => $item->date_issued,
            ]);
        }
        exit;

        // 1. User
        $users = \App\User::all()->map(function($user) {
            $user->update([
                'password' => app('hash')->make('password123'),
            ]);
        });

        // 2. Outgoing
        \App\OutgoingLetter::all()->map(function($item) {
            $update = [];
            $fields = [
                'tanggal',
                'tanggal_po',
            ];

            foreach ($fields as $field) {
                if (strlen($item[$field]) == 10) {
                    $update[$field] = substr($item[$field], 6, 4) . '-' . substr($item[$field], 3, 2) . '-' . substr($item[$field], 0, 2);
                }
            }

            $item->update($update);
        });

        // 3. Income
        \App\IncomingLetter::all()->map(function($item) {
            $update = [];
            $fields = [
                'tanggal_terima',
                'tanggal_surat',
            ];

            foreach ($fields as $field) {
                if (strlen($item[$field]) == 10) {
                    $update[$field] = substr($item[$field], 6, 4) . '-' . substr($item[$field], 3, 2) . '-' . substr($item[$field], 0, 2);
                }
            }

            $item->update($update);
        });

        // 4. Proforma
        \App\ProformaInvoice::all()->map(function($item) {
            $update = [];
            $fields = [
                'tanggal',
                'date_issued',
                'date_due',
                'do_date',
                'invoice_date',
            ];

            foreach ($fields as $field) {
                if (strlen($item[$field]) == 10) {
                    $update[$field] = substr($item[$field], 6, 4) . '-' . substr($item[$field], 3, 2) . '-' . substr($item[$field], 0, 2);
                }
            }

            $item->update($update);
        });

        // 5. Invoice
        \App\InvoiceCustomer::all()->map(function($item) {
            $update = [];
            $fields = [
                'tanggal',
                'date_issued',
                'date_due',
                'do_date',
                'tgl_faktur_pajak',
            ];

            foreach ($fields as $field) {
                if (strlen($item[$field]) == 10) {
                    $update[$field] = substr($item[$field], 6, 4) . '-' . substr($item[$field], 3, 2) . '-' . substr($item[$field], 0, 2);
                }
            }

            $item->update($update);
        });

        // 6. Purchase
        \App\Purchase::all()->map(function($item) {
            $update = [];
            $fields = [
                'tanggal',
            ];

            foreach ($fields as $field) {
                if (strlen($item[$field]) == 10) {
                    $update[$field] = substr($item[$field], 6, 4) . '-' . substr($item[$field], 3, 2) . '-' . substr($item[$field], 0, 2);
                }
            }

            $item->update($update);
        });

        // 7. Quotation
        \App\Quotation::all()->map(function($item) {
            $update = [];
            $fields = [
                'tanggal',
            ];

            foreach ($fields as $field) {
                if (strlen($item[$field]) == 10) {
                    $update[$field] = substr($item[$field], 6, 4) . '-' . substr($item[$field], 3, 2) . '-' . substr($item[$field], 0, 2);
                }
            }

            $item->update($update);
        });

        // 8. Request
        \App\Request::all()->map(function($item) {
            $update = [];
            $fields = [
                'date',
            ];

            foreach ($fields as $field) {
                if (strlen($item[$field]) == 10) {
                    $update[$field] = substr($item[$field], 6, 4) . '-' . substr($item[$field], 3, 2) . '-' . substr($item[$field], 0, 2);
                }
            }

            $item->update($update);
        });


    }
}
