-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_administration 
USE `eoffice`;

-- Dumping structure for table db_administration.tbl_reminder_checklist
CREATE TABLE IF NOT EXISTS `tbl_reminder_checklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memo` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  `tanggal_reminder` date DEFAULT NULL,
  `tanggal_update` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table db_administration.tbl_reminder_checklist: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_reminder_checklist` DISABLE KEYS */;
REPLACE INTO `tbl_reminder_checklist` (`id`, `memo`, `deskripsi`, `tanggal_reminder`, `tanggal_update`, `id_user`) VALUES
	(6, 'Service AC', 'AC Daikin', '2020-09-09', '2020-09-25 03:16:42', 27505002),
	(7, 'Test 1', 'Test', '2020-10-15', '2020-09-25 03:23:35', 27505002),
	(8, 'STNK', 'perpanjang', '2020-09-28', '2020-09-25 03:48:25', 27505002),
	(9, 'Test 1', 'Test', '2020-10-15', '2020-09-25 03:50:53', 27505002);
/*!40000 ALTER TABLE `tbl_reminder_checklist` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
