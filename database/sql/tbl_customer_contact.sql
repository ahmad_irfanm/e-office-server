-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_administration
CREATE DATABASE IF NOT EXISTS `db_administration` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_administration`;

-- Dumping structure for table db_administration.tbl_customer_contact
CREATE TABLE IF NOT EXISTS `tbl_customer_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_administration.tbl_customer_contact: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_customer_contact` DISABLE KEYS */;
REPLACE INTO `tbl_customer_contact` (`id`, `customer_id`, `name`, `position`, `phone`, `email`) VALUES
	(1, 3, 'Ibu Hanny Purwaningsih ', 'Finance AP', '021-57959641', 'Hpurwaning@xl.co.id'),
	(2, 3, 'Ibu Linda Magdalena', 'Finance AP', '02-57959149', 'lindam@xl.co.id');
/*!40000 ALTER TABLE `tbl_customer_contact` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
