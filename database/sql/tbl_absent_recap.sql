-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

 
USE `eoffice`;

-- Dumping structure for table db_administration.tbl_absent_recap
CREATE TABLE IF NOT EXISTS `tbl_absent_recap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `nik` bigint(20) NOT NULL DEFAULT '0',
  `status` enum('Alfa','Sakit','Izin','Hari Libur') NOT NULL DEFAULT 'Alfa',
  `description` text,
  `updated_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table db_administration.tbl_absent_recap: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_absent_recap` DISABLE KEYS */;
REPLACE INTO `tbl_absent_recap` (`id`, `tanggal`, `nik`, `status`, `description`, `updated_by`) VALUES
	(1, '2020-08-27', 68816032, 'Izin', 'Ke rumah nenek', 'Andini'),
	(2, '2020-08-20', 49918045, 'Izin', 'Ke rumah nenek', NULL);
/*!40000 ALTER TABLE `tbl_absent_recap` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
