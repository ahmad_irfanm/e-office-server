-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for db_administration
DROP DATABASE IF EXISTS `db_administration`;
CREATE DATABASE IF NOT EXISTS `db_administration` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_administration`;

-- Dumping structure for table db_administration.tbl_customer
DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `address` text,
  `npwp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table db_administration.tbl_customer: ~28 rows (approximately)
/*!40000 ALTER TABLE `tbl_customer` DISABLE KEYS */;
REPLACE INTO `tbl_customer` (`id`, `name`, `address`, `npwp`) VALUES
	(3, 'PT. XL Axiata Tbk.', NULL, '01.345.276.8-092.000'),
	(4, 'PT. Indosat, Tbk.', NULL, '01.000.502.3-092.000'),
	(5, 'PT. Telemedia Mitra Erajaya', NULL, '02.094.607.5-075.000'),
	(6, 'PT. Smart Telecom', NULL, '01.792.185.9-073.000'),
	(7, 'PT. Bali Towerindo Sentra', NULL, '02.601.536.2-904.000'),
	(8, 'PT. Sampoerna Telekomunikasi Indonesia', NULL, '01.764.485.7-058.000'),
	(9, 'PT. Telekomunikasi  Indonesia, Tbk.', NULL, '01.000.013.1-051.000'),
	(10, 'PT. Alcatel-Lucent Indonesia', NULL, '01.071.914.4-058.000'),
	(11, 'CV. Elmaga Tech', NULL, ''),
	(12, 'PT. Interlink Technology', NULL, ''),
	(13, 'PT. Telekomunikasi Selular ', NULL, '01.718.327.8-093.000'),
	(14, 'PT. Indonesia Comnets Plus', NULL, '01.061.190.3.051.000 '),
	(15, 'Mediatron', NULL, '01.831.784.2-073.000'),
	(16, 'PT. Khrista Dasetra', NULL, '01.960.262.2-073.000 '),
	(17, 'PT. Prastiwahyu Trimitra Engineering', NULL, '02.319.305.5-007.000'),
	(18, 'PT. Berca Handayaperkasa', NULL, '01.541.771.0-073.000'),
	(19, 'PT. Packet Systems Indonesia', NULL, '02.418.882.3-059.000'),
	(20, 'PT Next Generation Wave', NULL, '02.750.690.6-039.000'),
	(21, 'PT Axis Telekom Indonesia', NULL, '02.015.460.5-058.000'),
	(22, 'PT Mobilkom Telekomindo', NULL, '01.070.618.2-058.000'),
	(23, 'PT Trijaya Mandiri Sukses', NULL, '02.751.242.5-039.000'),
	(24, 'PT Hutchison 3 Indonesia', NULL, '01.967.397.9-092.000'),
	(25, 'PT. Astra International Tbk.', NULL, '01.302.584.6-092.000'),
	(26, 'PT. Huawei Tech Investment', NULL, '02.116.379.5-092.000'),
	(27, 'PT. Royal Abadi Sejahtera', NULL, '01.455.909.0-441.000'),
	(28, 'PT. Anugrah Cemerlang Abadi', NULL, '66.327.032.0-451.000'),
	(29, 'PT. Bestari Mulia', NULL, '02.638.198.8-603.000'),
	(30, 'PT. Scherling Indonesia', NULL, '02.568.722.9-424.000');
/*!40000 ALTER TABLE `tbl_customer` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
