<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return view('test');
});

$router->get('/test-connection', function () use ($router) {
    try {
        DB::connection()->getPdo();
        echo "Connected successfully to: " . DB::connection()->getDatabaseName();
    } catch (\Exception $e) {
        die("Could not connect to the database. Please check your configuration. error:" . $e );
    }
});

$router->post('/test', 'ReminderController@send_mail');

$router->get('/key', function () {
    return app('hash')->make('password');
});

$router->group(['prefix' => '/api'], function () use ($router) {
   $router->group(['prefix' => '/v1'], function () use ($router) {

       $router->group(['prefix' => '/dashboard', 'middleware' => []], function () use ($router) {
          $router->get('/punctuality-now/{type}', 'DashboardController@punctuality_now');
          $router->get('/summary', 'DashboardController@summary');
       });

       $router->group(['prefix' => '/auth', 'middleware' => ['cors']], function () use ($router) {
          $router->post('/login', 'AuthController@login');
          $router->post('/change-password', 'AuthController@change_password');
          $router->post('/reset-password', 'AuthController@reset_password');
       });

       $router->group(['prefix' => '/customer'], function () use ($router) {
           $router->get('/', 'CustomerController@index');
           $router->get('/{id}', 'CustomerController@show');
           $router->post('/', 'CustomerController@store');
           $router->put('/{id}', 'CustomerController@update');
           $router->delete('/{id}', 'CustomerController@destroy');
       });

       $router->group(['prefix' => '/customer-contact'], function () use ($router) {
           $router->get('/', 'CustomerContactController@index');
           $router->get('/{id}', 'CustomerContactController@show');
           $router->post('/', 'CustomerContactController@store');
           $router->put('/{id}', 'CustomerContactController@update');
           $router->delete('/{id}', 'CustomerContactController@destroy');
       });

       $router->group(['prefix' => '/incoming-letters'], function () use ($router) {
           $router->get('/', 'IncomingLetterController@index');
           $router->get('/{id}', 'IncomingLetterController@show');
           $router->post('/', 'IncomingLetterController@store');
           $router->put('/{id}', 'IncomingLetterController@update');
           $router->delete('/{id}', 'IncomingLetterController@destroy');
       });

       $router->group(['prefix' => '/outgoing-letters'], function () use ($router) {
           $router->get('/last-number', 'OutgoingController@get_last_number');

           $router->get('/', 'OutgoingController@index');
           $router->get('/{id}', 'OutgoingController@show');
           $router->post('/', 'OutgoingController@store');
           $router->put('/{id}', 'OutgoingController@update');
           $router->delete('/{id}', 'OutgoingController@destroy');

       });

       $router->group(['prefix' => '/invoice-customers'], function () use ($router) {
           $router->post('/{id}/upload', 'InvoiceCustomerController@upload');

           $router->get('/{id}/download-invoice', 'InvoiceCustomerController@download_invoice');
           $router->get('/{id}/print-invoice', 'InvoiceCustomerController@print_invoice');



            $router->get('/', 'InvoiceCustomerController@index');
            $router->get('/{id}', 'InvoiceCustomerController@show');
            $router->post('/', 'InvoiceCustomerController@store');
            $router->put('/{id}', 'InvoiceCustomerController@update');
            $router->delete('/{id}', 'InvoiceCustomerController@destroy');



       });

       $router->group(['prefix' => '/proforma-invoices'], function () use ($router) {
           $router->post('/{id}/upload', 'ProformaInvoiceController@upload');
           $router->get('/', 'ProformaInvoiceController@index');
           $router->get('/{id}', 'ProformaInvoiceController@show');
           $router->post('/', 'ProformaInvoiceController@store');
           $router->put('/{id}', 'ProformaInvoiceController@update');
           $router->delete('/{id}', 'ProformaInvoiceController@destroy');
       });

       $router->group(['prefix' => '/purchases'], function () use ($router) {
           $router->post('/{id}/upload', 'PurchaseController@upload');
           $router->get('/', 'PurchaseController@index');
           $router->get('/{id}', 'PurchaseController@show');
           $router->post('/', 'PurchaseController@store');
           $router->put('/{id}', 'PurchaseController@update');
           $router->delete('/{id}', 'PurchaseController@destroy');
       });

       $router->group(['prefix' => '/purchase-principles'], function () use ($router) {
           $router->post('/{id}/upload', 'PurchasePrincipleController@upload');
           $router->get('/', 'PurchasePrincipleController@index');
           $router->get('/{id}', 'PurchasePrincipleController@show');
           $router->post('/', 'PurchasePrincipleController@store');
           $router->put('/{id}', 'PurchasePrincipleController@update');
           $router->delete('/{id}', 'PurchasePrincipleController@destroy');
       });

       $router->group(['prefix' => '/quotations'], function () use ($router) {
           $router->get('/', 'QuotationController@index');
           $router->get('/{id}', 'QuotationController@show');
           $router->put('/{id}', 'QuotationController@update');
           $router->post('/', 'QuotationController@store');
           $router->delete('/{id}', 'QuotationController@destroy');
       });

       $router->group(['prefix' => '/letter-codes'], function () use ($router) {
          $router->get('/', 'LetterCodeController@index');
       });

       $router->group(['prefix' => '/requests'], function () use ($router) {
           $router->get('/', 'RequestController@index');
           $router->post('/', 'RequestController@store');
           $router->put('/{id}', 'RequestController@update');
           $router->put('/{id}/finish', 'RequestController@finish');

           $router->get('/drivers', 'RequestController@get_all_drivers');
           $router->get('/messengers', 'RequestController@get_all_messengers');
       });

       $router->group(['prefix' => '/reminders'], function () use ($router) {
            $router->get('/', 'ReminderController@index');
            $router->get('/histories', 'ReminderController@get_histories');

            $router->delete('/histories', 'ReminderController@remove_histories');
            $router->get('/notification', 'ReminderController@notification');

            $router->get('/{id}', 'ReminderController@show');
            $router->post('/', 'ReminderController@store');
            $router->put('/{id}', 'ReminderController@update');
            $router->post('/{id}/check', 'ReminderController@check');
            $router->delete('/{id}', 'ReminderController@destroy');
            $router->post('/send-mail', 'ReminderController@send_mail');
       });

       $router->group(['prefix' => '/inventories'], function () use ($router) {
           $router->get('/', 'InventoryController@index');
       });

       $router->group(['prefix' => '/employees'], function () use ($router) {
           $router->get('/', 'EmployeeController@index');
           $router->get('/{id}', 'EmployeeController@show');
           $router->post('/', 'EmployeeController@store');
           $router->put('/{id}', 'EmployeeController@update');
           $router->delete('/{id}', 'EmployeeController@destroy');
       });

       $router->group(['prefix' => '/family'], function () use ($router) {
           $router->get('/', 'FamilyController@index');
           $router->get('/{id}', 'FamilyController@show');
           $router->post('/', 'FamilyController@store');
           $router->put('/{id}', 'FamilyController@update');
           $router->delete('/{id}', 'FamilyController@destroy');
       });

       $router->group(['prefix' => '/emergency'], function () use ($router) {
           $router->get('/', 'EmergencyController@index');
           $router->get('/{id}', 'EmergencyController@show');
           $router->post('/', 'EmergencyController@store');
           $router->put('/{id}', 'EmergencyController@update');
           $router->delete('/{id}', 'EmergencyController@destroy');
       });

       $router->group(['prefix' => '/education'], function () use ($router) {
           $router->get('/', 'EducationController@index');
           $router->get('/{id}', 'EducationController@show');
           $router->post('/', 'EducationController@store');
           $router->put('/{id}', 'EducationController@update');
           $router->delete('/{id}', 'EducationController@destroy');
       });

       $router->group(['prefix' => '/experience'], function () use ($router) {
           $router->get('/', 'ExperienceController@index');
           $router->get('/{id}', 'ExperienceController@show');
           $router->post('/', 'ExperienceController@store');
           $router->put('/{id}', 'ExperienceController@update');
           $router->delete('/{id}', 'ExperienceController@destroy');
       });

       $router->group(['prefix' => '/certificate'], function () use ($router) {
           $router->get('/', 'CertificateController@index');
           $router->get('/{id}', 'CertificateController@show');
           $router->post('/{id}/upload', 'CertificateController@store');
           $router->put('/{id}', 'CertificateController@update');
           $router->delete('/{id}', 'CertificateController@destroy');
       });

       $router->group(['prefix' => '/absents'], function () use ($router) {

           // test
           $router->get('/test', 'AbsentControllers\AbsentController@test');
           $router->post('/upload', 'AbsentControllers\AbsentController@upload');

           $router->get('/not-present', 'AbsentControllers\AbsentController@not_present');
           $router->post('/not-present/update-status', 'AbsentControllers\AbsentController@update_status_not_present');

           $router->get('/not-present/chart', 'AbsentControllers\AbsentController@not_present_chart');

           // punctuality chart all employee between start date and end date
           $router->get('/punctuality-chart/{start_date}/{end_date}', 'AbsentControllers\AbsentController@get_punctuality_chart');

           // employee list of scan per day
           $router->get('/employee/scan/{nik}/{year}', 'AbsentControllers\AbsentController@get_employee_scan');

           // employee pie chart of punctuality in a year
           $router->get('/employee/pie/{nik}/{year}', 'AbsentControllers\AbsentController@get_employee_pie');

           // employee punctuality chart in a year by months
           $router->get('/employee/punctuality/{nik}/{year}', 'AbsentControllers\AbsentController@get_employee_punctuality');

           // employee time came chart in a year
           $router->get('/employee/time-came/{nik}/{year}', 'AbsentControllers\AbsentController@get_employee_timecame');

       });

       $router->group(['prefix' => '/technical'], function () use ($router) {

           $router->group(['prefix' => '/equipment'], function () use ($router) {
               $router->group(['prefix' => '/brand'], function () use ($router) {
                   $router->get('/', 'EquipmentControllers\BrandController@index');
                   $router->get('/{id}', 'EquipmentControllers\BrandController@show');
                   $router->post('/', 'EquipmentControllers\BrandController@store');
                   $router->put('/{id}', 'EquipmentControllers\BrandController@update');
                   $router->delete('/{id}', 'EquipmentControllers\BrandController@destroy');
               });

               $router->group(['prefix' => '/equipment'], function () use ($router) {
                   $router->get('/', 'EquipmentControllers\EquipmentController@index');
                   $router->get('/location', 'EquipmentControllers\EquipmentController@location');
                   $router->get('/delivered', 'EquipmentControllers\EquipmentController@delivered');
                   $router->get('/{id}', 'EquipmentControllers\EquipmentController@show');
                   $router->post('/', 'EquipmentControllers\EquipmentController@store');
                   $router->put('/{id}', 'EquipmentControllers\EquipmentController@update');
                   $router->delete('/{id}', 'EquipmentControllers\EquipmentController@destroy');
               });
           });


           $router->group(['prefix' => '/techteam-report'], function () use ($router) {
               $router->get('/', 'TechteamReportController@index');
               $router->get('/{id}', 'TechteamReportController@show');
               $router->post('/', 'TechteamReportController@store');
               $router->put('/{id}', 'TechteamReportController@update');
               $router->delete('/{id}', 'TechteamReportController@destroy');
           });

           $router->group(['prefix' => '/monthly-report'], function () use ($router) {
               $router->get('/', 'MonthlyReportController@index');
               $router->get('/{id}', 'MonthlyReportController@show');
               $router->post('/', 'MonthlyReportController@store');
               $router->put('/{id}', 'MonthlyReportController@update');
               $router->delete('/{id}', 'MonthlyReportController@destroy');
           });

           $router->group(['prefix' => '/charged-units'], function () use ($router) {
               $router->get('/', 'ChargedUnitController@index');
               $router->get('/{id}', 'ChargedUnitController@show');
               $router->post('/', 'ChargedUnitController@store');
               $router->put('/{id}', 'ChargedUnitController@update');
               $router->delete('/{id}', 'ChargedUnitController@destroy');
           });

           $router->group(['prefix' => '/execution-id'], function () use ($router) {
               $router->get('/', 'ExecutionIDController@index');
               $router->get('/{id}', 'ExecutionIDController@show');
               $router->post('/', 'ExecutionIDController@store');
               $router->put('/{id}', 'ExecutionIDController@update');
               $router->delete('/{id}', 'ExecutionIDController@destroy');
           });


       });

       $router->group(['prefix' => '/sales'], function () use ($router) {
           $router->group(['prefix' => '/forecasts'], function () use ($router) {

               $router->get('/graph/bar', 'MarketingControllers\ForecastController@graph_bar');
               $router->get('/', function () {
                    return response()->json(\App\MarketingModels\Forecast::orderBy('project_name', 'asc')->get(), 200);
                });
               $router->get('/{year}', 'MarketingControllers\ForecastController@index');
               $router->get('/{id}/detail', 'MarketingControllers\ForecastController@show');
               $router->post('/', 'MarketingControllers\ForecastController@store');
               $router->put('/{id}', 'MarketingControllers\ForecastController@update');
               $router->delete('/{id}', 'MarketingControllers\ForecastController@destroy');
           });

           $router->group(['prefix' => '/customer-contacts'], function () use ($router) {

               $router->get('/overview', 'MarketingControllers\CustomerContactController@overview');

                $router->get('/', 'MarketingControllers\CustomerContactController@index');
                $router->get('/{id}', 'MarketingControllers\CustomerContactController@show');
                $router->post('/', 'MarketingControllers\CustomerContactController@store');
                $router->put('/{id}', 'MarketingControllers\CustomerContactController@update');
                $router->delete('/{id}', 'MarketingControllers\CustomerContactController@destroy');
           });

           $router->group(['prefix' => '/company'], function () use ($router) {
               $router->get('/', 'MarketingControllers\CompanyController@index');
               $router->get('/{id}', 'MarketingControllers\CompanyController@show');
               $router->post('/', 'MarketingControllers\CompanyController@store');
               $router->put('/{id}', 'MarketingControllers\CompanyController@update');
               $router->delete('/{id}', 'MarketingControllers\CompanyController@destroy');
           });

           $router->group(['prefix' => '/visit-report'], function () use ($router) {
               $router->get('/', 'MarketingControllers\VisitReportController@index');
               $router->get('/{id}', 'MarketingControllers\VisitReportController@show');
               $router->post('/', 'MarketingControllers\VisitReportController@store');
               $router->put('/{id}', 'MarketingControllers\VisitReportController@update');
               $router->delete('/{id}', 'MarketingControllers\VisitReportController@destroy');
           });

           $router->group(['prefix' => '/proses-order'], function () use ($router) {
               $router->get('/', 'MarketingControllers\ProsesOrderController@index');
               $router->get('/{id}', 'MarketingControllers\ProsesOrderController@show');
               $router->post('/', 'MarketingControllers\ProsesOrderController@store');
               $router->put('/{id}', 'MarketingControllers\ProsesOrderController@update');
               $router->delete('/{id}', 'MarketingControllers\ProsesOrderController@destroy');
           });

           $router->group(['prefix' => '/level'], function () use ($router) {
               $router->get('/', 'MarketingControllers\LevelController@index');
               $router->get('/{id}', 'MarketingControllers\LevelController@show');
               $router->post('/', 'MarketingControllers\LevelController@store');
               $router->put('/{id}', 'MarketingControllers\LevelController@update');
               $router->delete('/{id}', 'MarketingControllers\LevelController@destroy');
           });

           $router->group(['prefix' => '/user'], function () use ($router) {
               $router->get('/statistic', 'MarketingControllers\UserController@statistic');
               $router->get('/', 'MarketingControllers\UserController@index');
               $router->get('/{id}', 'MarketingControllers\UserController@show');
               $router->post('/', 'MarketingControllers\UserController@store');
               $router->put('/{id}', 'MarketingControllers\UserController@update');
               $router->delete('/{id}', 'MarketingControllers\UserController@destroy');

           });

           $router->group(['prefix' => '/merk'], function () use ($router) {
               $router->get('/', 'MarketingControllers\MerkController@index');
               $router->get('/{id}', 'MarketingControllers\MerkController@show');
               $router->post('/', 'MarketingControllers\MerkController@store');
               $router->put('/{id}', 'MarketingControllers\MerkController@update');
               $router->delete('/{id}', 'MarketingControllers\MerkController@destroy');
           });
       });

       $router->group(['prefix' => '/jobs'], function () use ($router) {
           $router->get('/status/{status}', 'JobController@get_by_status');
           $router->get('/{year}', 'JobController@index');
           $router->get('/{id}/detail', 'JobController@show');
           $router->post('/', 'JobController@store');
           $router->put('/{id}', 'JobController@update');
           $router->delete('/{id}', 'JobController@destroy');
       });

       $router->group(['prefix' => '/todo-lists'], function () use ($router) {
           $router->get('/date/{date}', 'TodoListController@index');
           $router->get('/{id}/detail', 'TodoListController@show');
           $router->post('/', 'TodoListController@store');
           $router->put('/{id}', 'TodoListController@update');
           $router->delete('/{id}', 'TodoListController@destroy');
       });

       $router->group(['prefix' => '/view-reports', 'middleware' => ['auth:api']], function () use ($router) {
           $router->get('/{date_start}/{date_end}', 'ReportController@index');
           $router->get('/{id}/detail', 'ReportController@show');
           $router->post('/', 'TodoListController@store');
       });

       $router->group(['prefix' => '/user'], function () use ($router) {
           $router->get('/', 'UserController@index');
           $router->get('/{id}', 'UserController@show');
           $router->post('/', 'UserController@store');
           $router->put('/{id}', 'UserController@update');
           $router->delete('/{id}', 'UserController@destroy');

       });

       $router->get('/positions', function () {
          return response()->json([
              'message' => 'Data loaded successful',
              'values' => \App\Position::get()
          ], 200);
       });

   });
});
