<?php

return [

    'default' => 'mysql',

    'connections' => [
        'mysql' => [
            'driver'    => 'mysql',
            'host'      => env('DB_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB_DATABASE'),
            'username'  => env('DB_USERNAME'),
            'password'  => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'absent' => [
            'driver'    => 'mysql',
            'host'      => env('DB2_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB2_DATABASE'),
            'username'  => env('DB2_USERNAME'),
            'password'  => env('DB2_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'equipment' => [
            'driver'    => 'mysql',
            'host'      => env('DB3_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB3_DATABASE'),
            'username'  => env('DB3_USERNAME'),
            'password'  => env('DB3_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'marketing' => [
            'driver'    => 'mysql',
            'host'      => env('DB4_HOST'),
            'port'      => env('DB_PORT'),
            'database'  => env('DB4_DATABASE'),
            'username'  => env('DB4_USERNAME'),
            'password'  => env('DB4_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'backup' => [
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'port'      => env('DB_PORT'),
            'database'  => env('DB5_DATABASE'),
            'username'  => env('DB5_USERNAME'),
            'password'  => env('DB5_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

    ],
];
