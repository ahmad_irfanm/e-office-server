<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice Customer</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@400;700&display=swap');
        * {
            font-family: 'Source Serif Pro', serif;
            box-sizing: border-box;
        }
        body {
            font-size: 12px;
            margin: 0 auto;
        }
        .wrapper {
            margin: 0 auto;
            /*width: 600px;*/
            /*border: 1px solid #333;*/
            padding: 32px;
        }

        /*header*/
        .header {
            display: grid;
            grid-template-columns: repeat(2, auto);
            align-items: center;
            justify-content: space-between;
            margin-bottom: 20px;
        }
        .logo {
            grid-column: 1/3;
        }
        .logo img {
            width: 100px;
        }
        .meta {
            border-collapse: collapse;
        }
        .meta th {
            background: darkgray;
        }
        .meta th, .meta td {
            padding: 8px;
            border: 1px solid #808080;
        }

        .info {
            grid-column: 1/3;
            width: 100%;
            border: 1px solid #808080;
            margin-top: 20px;
            display: grid;
            grid-template-columns: repeat(2, 1fr);
        }
        .info-right {
            padding: 12px;
            border-left: 1px solid #808080;
        }
        .info-left {
            padding: 12px;
        }

        .goods {
            width: 100%;
            border-collapse: collapse;
        }
        .goods th {
            background: darkgray;
        }
        .goods th, .goods td {
            padding: 8px;
            border: 1px solid #808080;
        }

        .goods tbody td {
            border-bottom: 1px solid transparent;
        }
        .goods tbody tr:last-child td {
            border-bottom: 1px solid #808080;
        }

        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .bold {
            font-weight: bold;
        }

        .bottom {
            margin-top: 20px;
            display: grid;
            grid-template-columns: 75% 25%;
            align-items: start;
            justify-content: space-between;
        }
        .signature {
            text-align: center;
            margin-top: 60px;
        }
        .signature-caption {
            margin-top: 60px;
        }
        .signature-caption .name {
            text-decoration: underline;
            margin: 0px;
            padding: 0px;
            line-height: 1;
        }

        .note {
            margin-top: 60px;
        }

    </style>
</head>
<body>

    <div class="wrapper">

        <header class="header">

            <div class="logo">
                <img src="{{url('assets/argasolusi.png')}}" alt="logo-arga">
            </div>

            <h1 class="title">
                INVOICE
            </h1>

            <table class="meta">
                <tr>
                    <th>INVOICE NUMBER</th>
                    <th>DATE</th>
                </tr>
                <tr>
                    <td>{{$item->invoice_number}}</td>
                    <td>{{date('d/m/Y', strtotime($item->tanggal))}}</td>
                </tr>
            </table>

            <div class="info">
                <div class="info-left">
                    <h4 style="margin: 0px; padding: 0px;">Kepada Yth:</h4>
                    <p>{{$item->customer}}</p>
                </div>
                <div class="info-right">
                    <table width="100%;">
                        <tr>
                            <td><h4 style="margin: 0px; padding: 0px;">Ref. Quotatoin No.</h4></td>
                            <td>:</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><h4 style="margin: 0px; padding: 0px;">Ref. Your PO. No.</h4></td>
                            <td>:</td>
                            <td>{{$item->no_po_kontrak}}</td>
                        </tr>
                        <tr>
                            <td><h4 style="margin: 0px; padding: 0px;">Date PO</h4></td>
                            <td>:</td>
                            <td>{{date('d/m/Y', strtotime($item->po_date))}}</td>
                        </tr>
                    </table>
                </div>
            </div>

        </header>

        <main class="content">

            <table class="goods">
                <thead>
                    <tr>
                        <th class="text-center" width="5%">No.</th>
                        <th class="text-center" width="10%">Jumlah Quantity</th>
                        <th width="35%">Uraian Barang <br> Description of goods</th>
                        <th width="25%" class="text-right">Harga per Unit (IDR) <br> Unit Price (IDR)</th>
                        <th width="25%" class="text-right">Jumlah Harga (IDR) <br> Total Price (IDR)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($item->goods as $i => $goods)
                        <tr>
                            <td class="text-center">{{$i + 1}}</td>
                            <td class="text-center">{{$goods->amount}}</td>
                            <td>{{isset($goods->name) ? $goods->name : '-'}}</td>
                            <td class="text-right">{{number_format($goods->price, 0)}}</td>
                            <td class="text-right">{{number_format($goods->total, 0)}}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" rowspan="3"></td>
                        <td class="text-right bold">Sub Total</td>
                        <td class="text-right bold">{{number_format($item->total, 0)}}</td>
                    </tr>
                    <tr>
                        <td class="text-right bold">VAT 10%</td>
                        <td class="text-right bold">{{number_format($item->vat, 0)}}</td>
                    </tr>
                    <tr>
                        <td class="text-right bold" style="font-size: 14px;">Grand Total</td>
                        <td class="text-right bold" style="font-size: 14px;">{{number_format($item->subtotal, 0)}}</td>
                    </tr>
                </tfoot>
            </table>

            <div class="bottom">

                <div class="description">
                    <h4 style="margin: 0px; padding: 0px;">Keterangan: </h4>
                    <p>{{$item->keterangan}}</p>
                </div>

                <div class="signature">

                    <div class="signature-date">
                        Jakarta, {{date('d/m/Y')}}
                    </div>

                    <div class="signature-caption">
                        <h3 class="name">Ira Mulyati</h3>
                        <p class="position">Direktur</p>
                    </div>

                </div>

            </div>

            <div class="note">
                <p>
                    Atas perhatiannya, kami ucapkan terimakasih
                </p>
                <p>
                    Catatan: <br>
                    Pembarayan dapat dilakukan dengan transfer ke rekening kami : <br>
                    PT. Arga Solusi Technologies <br>
                    Bank BRI <br>
                </p>
            </div>

        </main>



    </div>

</body>
</html>
