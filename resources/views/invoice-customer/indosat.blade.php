<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            font-size: 12px;
        }
        .header {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 1rem;
        }
        .header td {
        }
        .meta {
            border-collapse: collapse;
            width: 100%;
        }
        .meta th, .meta td {
            border: 2px solid #333;
            padding: 8px 16px;
        }
        .meta th {
            background: #ccc;
        }
        .intro {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 1rem;
        }
        .intro td {
            width: 50%;
            border: 2px solid #333;
            vertical-align: top;
            padding: 8px;
        }
        .intro table {
            border-collapse: collapse;
        }
        .intro table td {
            border: 0px;
            width: auto;
            padding: 2px 8px;
        }

        .desc-goods {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 1rem;
        }
        .desc-goods th {
            background: #ccc;
        }
        .desc-goods td,.desc-goods th {
            border: 2px solid #333;
            padding: 4px;
        }
        .desc-goods h4 {
            margin: 0;
        }

        .end {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 1rem;
        }
        .end td {
            vertical-align: top;
        }

        .info {
            margin-bottom: 2rem;
        }

        .footer {

        }
        .footer h4 {
            margin: 0;
        }
        .footer p {
            margin: 0;
        }


        .top {
            width: 100%;
        }
    </style>
</head>
<body>

<table class="top">
    <tr>
        <td>
            <div style="width: 80px; height: 80px; border: 1px solid #333;"></div>
        </td>
        <td align="right" style="font-size: 16px;">
           PO No. {{$item->no_po_kontrak}}
        </td>
    </tr>
</table>


<table class="header">
    <tr>
        <td style="width: 60%; vertical-align: bottom;">
            <h1>INVOICE</h1>
        </td>
        <td style="vertical-align: bottom;">
            <table class="meta">
                <tr>
                    <th align="center">INVOICE NUMBER</th>
                    <th align="center">DATE</th>
                </tr>
                <tr>
                    <td style="text-align: center;">{{str_replace('/', ' / ',$item->invoice_number)}}</td>
                    <td style="text-align: center;">{{date('d / m / Y', strtotime($item->invoice_date))}}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table class="intro">
    <tr>
        <td>
            Kepada Yth.
            <br>
            <h4 style="margin: 8px 0;">{{$item->customer}}</h4>
            <p>
                {{$item->customer_data ? $item->customer_data->address : '-'}}
            </p>
        </td>
    </tr>
</table>

<table class="desc-goods">
    <tr>
        <th>
            No.
        </th>
        <th>
            Jumlah Quantity
        </th>
        <th>
            Uraian Barang
        </th>
        <th>
            Harga per Unit (IDR) <br>
            Unit Price (IDR)
        </th>
        <th>
            Jumlah Harga (IDR) <br>
            Total Price (IDR)
        </th>
    </tr>

        @foreach ($item->goods as $i => $good)
            <tr>
                <td style="text-align: center;">
                    {{$i + 1}}
                </td>
                <td style="text-align: center;">
                    {{$good->amount}}
                </td>
                <td>
                    {{$good->name}}
                </td>
                <td style="text-align: right;">
                    {{number_format($good->price, 0)}}
                </td>
                <td style="text-align: right;">
                    {{number_format($good->total, 0)}}
                </td>
            </tr>
        @endforeach
    <tr>
        <td colspan="3" rowspan="3"></td>
        <td><h4>Sub Total</h4></td>
        <td style="text-align: right;">{{number_format($item->subtotal, 0)}}</td>
    </tr>
    <tr>
        <td><h4>VAT (10%)</h4></td>
        <td style="text-align: right;">{{number_format($item->vat, 0)}}</td>
    </tr>
    <tr>
        <td><h4>Total</h4></td>
        <td style="text-align: right;">{{number_format($item->total, 0)}}</td>
    </tr>
    <tr>
        <td colspan="5">
            Terbilang : # {{ $item->terbilang }} #
        </td>
    </tr>
</table>

<table class="end">
    <tr>
        <td style="width: 80%;">
        <p>Keterangan : </p>
        <p>{{$item->keterangan}}</p>
        </td>
        <td style="text-align: center;">
            <p style="margin-bottom: 4rem;">Jakarta, {{ date('d / m / Y') }}</p>

            <h4 style="margin-bottom: 0.3rem;"><u>Ira Mulyati</u></h4>
            <span>Direktur</span>
        </td>
    </tr>
</table>

<div class="info">
<p>Atas perhatiannya, kami ucapkan terima kasih <br/>
    Catatan : <br />
Pembayaran dapat dilakukan dengan transfer ke rekening kami : <br />
PT. Arga Solusi Technologies </p>

<table class="rek">
        <tr>
            <td>Rekening USD</td>
            <td>:</td>
            <td>{{$item->rek_usd}}</td>
        </tr>
        <tr>
            <td>Rekening Rupiah</td>
            <td>:</td>
            <td>{{$item->rek_rupiah}}</td>
        </tr>
</table>
</div>

<div class="footer">

<h4>PT ARGA SOLUSI TECHNOLOGIES</h4>
<p>Thamrin City office Park Blok AB 03, Jl. Kebon Kacang Raya, Kebon Melati - Jakarta 10230 <br />
Phone : 021-31990781 Fax : 021-31990783 <br />
Email : sales.center@argasolusi.com, Website : www.argasolusi.com
</p>
</div>

</body>
</html>
