<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice Customer</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Source+Serif+Pro:wght@400;700&display=swap');
        * {
            font-family: 'Source Serif Pro', serif;
            box-sizing: border-box;
        }
        body {
            font-size: 12px;
            margin: 0 auto;
        }
        .content {
            border-collapse: collapse;
        }

        /*header*/
        .header {
            position: relative;
            margin-bottom: 20px;
            height: 80px;
        }
        .logo {

        }
        .logo img {
            width: 100px;
        }
        .meta {
            border-collapse: collapse;
        }
        .meta th {
            background: darkgray;
        }
        .meta th, .meta td {
            padding: 8px;
            border: 1px solid #808080;
        }

        .info > td {
            border: 1px solid #808080;
            padding: 12px;
        }

        .top {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 20px;
        }
        .header > td {
            padding-bottom: 20px;
        }

        .content {
            margin-bottom: 20px;
        }
        .content > td {
            padding-top: 12px;
        }

        .goods {
            width: 100%;
            border-collapse: collapse;
        }
        .goods th {
            background: darkgray;
        }
        .goods th, .goods td {
            padding: 8px;
            border: 1px solid #808080;
        }

        .goods tbody td {
            border-bottom: 1px solid transparent;
        }
        .goods tbody tr:last-child td {
            border-bottom: 1px solid #808080;
        }

        .text-center {
            text-align: center;
        }
        .text-right {
            text-align: right;
        }
        .bold {
            font-weight: bold;
        }

        .signature {
            width: 100%;
            margin-bottom: 20px;
        }
        .signature-description {
            vertical-align: top;
            width: 65%;
        }
        .signature-date {
            margin-top: 24px;
        }
        .signature-caption {
            margin-top: 80px;
        }
        .signature-caption .name {
            text-decoration: underline;
            margin: 0px;
            padding: 0px;
            line-height: 1;
        }

        .payment {
            width: 100%;
            margin-bottom: 40px;
        }

        .footer {
            width: 100%;
        }

    </style>
</head>
<body>

<div class="wrapper">

    <table class="top">
        <tr>
            <td colspan="2">
                <div class="logo">
                </div>
            </td>
        </tr>

        <tr class="header">
            <td>
                <h1 class="title">
                    INVOICE
                </h1>
            </td>
            <td>
                <table class="meta" style="margin: 0 auto; margin-right: 0px;">
                    <tr>
                        <th>INVOICE NUMBER</th>
                        <th>DATE</th>
                    </tr>
                    <tr>
                        <td>{{$item->invoice_number}}</td>
                        <td>{{date('d/m/Y', strtotime($item->tanggal))}}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr class="info">
            <td style="width: 50%;">
                <h4 style="margin: 0px; padding: 0px;">Kepada Yth:</h4>
                <p>{{$item->customer}}</p>
            </td>
            <td style="width: 50%;">
                <table>
                    <tr>
                        <td><h4 style="margin: 0px; padding: 0px;">Ref. Quotatoin No.</h4></td>
                        <td>:</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><h4 style="margin: 0px; padding: 0px;">Ref. Your PO. No.</h4></td>
                        <td>:</td>
                        <td>{{$item->no_po_kontrak}}</td>
                    </tr>
                    <tr>
                        <td><h4 style="margin: 0px; padding: 0px;">Date PO</h4></td>
                        <td>:</td>
                        <td>{{date('d/m/Y', strtotime($item->po_date))}}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table style="width: 100%;" class="content">
        <tr>
            <td colspan="2">
                <table class="goods" style="width: 100%;">
                    <thead>
                    <tr>
                        <th class="text-center" width="5%">No.</th>
                        <th class="text-center" width="10%">Jumlah Quantity</th>
                        <th width="49%">Uraian Barang <br> Description of goods</th>
                        <th width="18%" class="text-right">Harga per Unit (IDR) <br> Unit Price (IDR)</th>
                        <th width="18%" class="text-right">Jumlah Harga (IDR) <br> Total Price (IDR)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($item->goods as $i => $goods)
                        <tr>
                            <td class="text-center">{{$i + 1}}</td>
                            <td class="text-center">{{$goods->amount}}</td>
                            <td>{{isset($goods->name) ? $goods->name : '-'}}</td>
                            <td class="text-right">{{number_format($goods->price, 0)}}</td>
                            <td class="text-right">{{number_format($goods->total, 0)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="3" rowspan="3"></td>
                        <td class="text-right bold">Sub Total</td>
                        <td class="text-right bold">{{number_format($item->total, 0)}}</td>
                    </tr>
                    <tr>
                        <td class="text-right bold">VAT 10%</td>
                        <td class="text-right bold">{{number_format($item->vat, 0)}}</td>
                    </tr>
                    <tr>
                        <td class="text-right bold" style="font-size: 14px;">Grand Total</td>
                        <td class="text-right bold" style="font-size: 14px;">{{number_format($item->subtotal, 0)}}</td>
                    </tr>
                    </tfoot>
                </table>
            </td>
        </tr>
    </table>

    <table class="signature">
        <tr>
            <td class="signature-description">
                <h4 style="margin: 0px; padding: 0px;">Keterangan: </h4>
                <p>{{$item->keterangan}}</p>
            </td>

            <td class="text-center">
                <div class="signature-date">
                    Jakarta, {{date('d/m/Y')}}
                </div>

                <div class="signature-caption">
                    <h3 class="name">Ira Mulyati</h3>
                    <p class="position">Direktur</p>
                </div>
            </td>
        </tr>
    </table>

    <table class="payment">
        <tr>
            <td colspan="2">
                <p>
                    Atas perhatiannya, kami ucapkan terimakasih
                </p>
                <p>
                    Catatan: <br>
                    Pembarayan dapat dilakukan dengan transfer ke rekening kami : <br>
                </p>
                <table>
                    <tr>
                        <td colspan="2">PT. Arga Solusi Technologies</td>
                    </tr>
                    <tr>
                        <td colspan="2">{{$item->rekening_pembayaran}}</td>
                    </tr>
                    <tr>
                        <td>Rekening USD</td>
                        <td>:</td>
                        <td>{{$item->rek_usd}}</td>
                    </tr>
                    <tr>
                        <td>Rekening Rupiah</td>
                        <td>:</td>
                        <td>{{$item->rek_rupiah}}</td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <div class="footer">
        <h4>
            PT ARGA SOLUSI TECHNOLOGIES
        </h4>
        <p>Thamrin City office Park Blok AB 03, Jl. Kebon Kacang Raya, Kebon Melati - Jakarta 10230</p>
        <p>Phone : 021-31990781 Fax : 021-31990783</p>
        <p>Email : sales.center@argasolusi.com, Website : www.argasolusi.com</p>
    </div>

</div>

</body>
</html>
