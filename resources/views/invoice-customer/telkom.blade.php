<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        body {
            font-size: 12px;
        }
        .header {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 1rem;
        }
        .header td {
        }
        .meta {
            border-collapse: collapse;
            width: 100%;
        }
        .meta th, .meta td {
            padding: 8px 16px;
        }
        .intro {
            border-collapse: collapse;
            width: 100%;
            margin-bottom: 1rem;
        }
        .intro td {
            width: 50%;
            border: 2px solid #333;
            vertical-align: top;
            padding: 8px;
        }
        .intro table {
            border-collapse: collapse;
        }
        .intro table td {
            border: 0px;
            width: auto;
            padding: 2px 8px;
        }

        .reference {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 1rem;
        }
        .reference th {
            text-align: left;
            padding: 4px;
        }
        .reference td {
            border: 2px solid #333;
            padding: 4px;
        }

        .top {
            width: 100%;
            margin-bottom: 1rem;
            border: 2px solid #333;
            text-align: center;
        }

        .desc-goods {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 1rem;
        }
        .desc-goods th {
            background: #ccc;
        }
        .desc-goods td,.desc-goods th {
            border: 2px solid #333;
            padding: 4px;
        }
        .desc-goods h4 {
            margin: 0;
        }

        .end {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 1rem;
        }
        .end td {
            vertical-align: top;
        }

        .info {
            margin-bottom: 2rem;
        }

        .footer {

        }
        .footer h4 {
            margin: 0;
        }
        .footer p {
            margin: 0;
        }

    </style>
</head>
<body>

<table class="top">
    <tr>
        <td>
            <h4>PT. ARGA SOLUSI TECHNOLOGIES</h4>
            <p>
                Thamrin City Office Park Blok AB 03, Jl. Kebon Kacang Raya, Kebon Melati - Jakarta 10230
            </p>
            <p>
                021-31990781
            </p>
            <p>
                sales.admin@argasolusi.com
            </p>
            <p>
                02.492.888.9-072.000
            </p>
        </td>
    </tr>
</table>

<table class="header">
    <tr>
        <td style="width: 60%; vertical-align: bottom;">
        </td>
        <td style="vertical-align: bottom;">
            <table class="meta">
                <tr>
                    <th align="right">INVOICE DATE</th>
                    <td>:</td>
                    <td>{{date('d / m / Y', strtotime($item->invoice_date))}}</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table class="intro">
    <tr>
        <td>
            Kepada Yth.
            <br>
            <h4 style="margin: 8px 0;">{{$item->customer}}</h4>
            <p>
                {{$item->customer_data ? $item->customer_data->address : '-'}}
            </p>
        </td>
        <td>
            <table>
                <tr>
                    <td>Ref. Quotatoin No.</td>
                    <td>:</td>
                    <td>
                        -
                    </td>
                </tr>
                <tr>
                    <td>Ref. Your PO No.</td>
                    <td>:</td>
                    <td>
                        {{$item->no_po_kontrak}}
                    </td>
                </tr>
                <tr>
                    <td>Date PO</td>
                    <td>:</td>
                    <td>
                        {{$item->po_date}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table class="reference">
    <tr>
        <th>
            <h2 style="margin: 0 0;">INVOICE</h2>
        </th>
    </tr>
    <tr>
        <th>
            Reference Buyer
        </th>
        <th>
            PO Number
        </th>
        <th>
            BAST Number
        </th>
        <th>
            Invoice No.
        </th>
        <th>
            Delivery Date
        </th>
    </tr>

    @foreach($item->additional as $add)
        <tr>
            <td>
                {{$add->buyer}}
            </td>
            <td>
                {{$add->po_number}}
            </td>
            <td>
                {{$add->bast_number}}
            </td>
            <td>
                {{$add->invoice_number}}
            </td>
            <td>
                {{$add->delivery_date}}
            </td>
        </tr>
    @endforeach
</table>

<table class="desc-goods">
    <tr>
        <th>
            No.
        </th>
        <th>
            Jumlah Quantity
        </th>
        <th>
            Uraian Barang
        </th>
        <th>
            Harga per Unit (IDR) <br>
            Unit Price (IDR)
        </th>
        <th>
            Jumlah Harga (IDR) <br>
            Total Price (IDR)
        </th>
    </tr>

        @foreach ($item->goods as $i => $good)
            <tr>
                <td style="text-align: center;">
                    {{$i + 1}}
                </td>
                <td style="text-align: center;">
                    {{$good->amount}}
                </td>
                <td>
                    {{$good->name}}
                </td>
                <td style="text-align: right;">
                    {{number_format($good->price, 0)}}
                </td>
                <td style="text-align: right;">
                    {{number_format($good->total, 0)}}
                </td>
            </tr>
        @endforeach
    <tr>
        <td colspan="3" rowspan="3"></td>
        <td><h4>Sub Total</h4></td>
        <td style="text-align: right;">{{number_format($item->subtotal, 0)}}</td>
    </tr>
    <tr>
        <td><h4>VAT (10%)</h4></td>
        <td style="text-align: right;">{{number_format($item->vat, 0)}}</td>
    </tr>
    <tr>
        <td><h4>Total</h4></td>
        <td style="text-align: right;">{{number_format($item->total, 0)}}</td>
    </tr>
    <tr>
        <td colspan="5">
            Terbilang : # {{ $item->terbilang }} #
        </td>
    </tr>
</table>

<table class="end">
    <tr>
        <td style="width: 80%;">
        <p>Keterangan : </p>
        <p>{{$item->keterangan}}</p>
        </td>
        <td style="text-align: center;">
            <p style="margin-bottom: 4rem;">Jakarta, {{ date('d / m / Y') }}</p>

            <h4 style="margin-bottom: 0.3rem;"><u>Ira Mulyati</u></h4>
            <span>Direktur</span>
        </td>
    </tr>
</table>

<div class="info">
<p>Atas perhatiannya, kami ucapkan terima kasih <br/>
    Catatan : <br />
Pembayaran dapat dilakukan dengan transfer ke rekening kami : <br />
PT. Arga Solusi Technologies </p>

<table class="rek">
        <tr>
            <td>Rekening USD</td>
            <td>:</td>
            <td>{{$item->rek_usd}}</td>
        </tr>
        <tr>
            <td>Rekening Rupiah</td>
            <td>:</td>
            <td>{{$item->rek_rupiah}}</td>
        </tr>
</table>
</div>

<div class="footer">

<h4>PT ARGA SOLUSI TECHNOLOGIES</h4>
<p>Thamrin City office Park Blok AB 03, Jl. Kebon Kacang Raya, Kebon Melati - Jakarta 10230 <br />
Phone : 021-31990781 Fax : 021-31990783 <br />
Email : sales.center@argasolusi.com, Website : www.argasolusi.com
</p>
</div>

</body>
</html>
